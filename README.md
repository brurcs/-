# Orsegups

Monorepo com todos os projetos de software da Orsegups em parceria com a INCUCA

## Instalar dependências dos pacotes

`npm install -g yarn && yarn install`

## Testar todos os pacotes

`yarn run test`

## Lint de todos os pacotes

`yarn run lint`

## Gerar nova versão para todos os pacotes

`yarn run bump`

## Abrir documentação

`yarn run doc`