(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["documents"],{

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Blank.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Blank.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
//
//
//
//
var _default = {
  name: 'Blank'
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Documents.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Documents.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

__webpack_require__(/*! core-js/modules/es6.regexp.replace */ "../../node_modules/core-js/modules/es6.regexp.replace.js");

__webpack_require__(/*! core-js/modules/es6.regexp.constructor */ "../../node_modules/core-js/modules/es6.regexp.constructor.js");

var _objectSpread2 = _interopRequireDefault(__webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js"));

var _vuex = __webpack_require__(/*! vuex */ "../../node_modules/vuex/dist/vuex.esm.js");

var _OrsInnerToolbar = _interopRequireDefault(__webpack_require__(/*! @/components/OrsInnerToolbar.vue */ "./src/components/OrsInnerToolbar.vue"));

var _OrsTimeline = _interopRequireDefault(__webpack_require__(/*! ../components/OrsTimeline.vue */ "./src/components/OrsTimeline.vue"));

var _OrsTimelineItem = _interopRequireDefault(__webpack_require__(/*! @/components/OrsTimelineItem.vue */ "./src/components/OrsTimelineItem.vue"));

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var session = (0, _vuex.createNamespacedHelpers)('session');
var docs = (0, _vuex.createNamespacedHelpers)('views/documents');
var _default = {
  name: 'Documents',
  components: {
    OrsInnerToolbar: _OrsInnerToolbar.default,
    OrsTimeline: _OrsTimeline.default,
    OrsTimelineItem: _OrsTimelineItem.default
  },
  computed: (0, _objectSpread2.default)({}, docs.mapGetters(['isLoading', 'noResults', 'noMoreResults']), docs.mapGetters(['limitedPeriods'])),
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.loadPeriods();
      vm.addAccountChangeListener(vm.loadPeriods);
    });
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    this.removeAccountChangeListener(this.loadPeriods);
    next();
  },
  methods: (0, _objectSpread2.default)({}, session.mapMutations(['addAccountChangeListener', 'removeAccountChangeListener']), docs.mapActions(['loadPeriods', 'loadMorePeriods']), {
    formatPeriod: function formatPeriod(period) {
      var str = period.competencia || '';
      return "Compet\xEAncia ".concat(str.substr(5, 2), "/").concat(str.substr(0, 4));
    },
    getTipo: function getTipo(doc) {
      var url = doc.urlDownload || '';
      var available = ['OUTROS', 'GRF', 'GPS', 'PCC', 'RTRET', 'FOPAG', 'RET', 'RE/', 'ERE', 'RFE', 'RFEF', 'RIPT', 'RAGRF', 'RAGPS', 'IRRF', 'DIRF', 'CONECSOCIAL'];
      var regex = new RegExp("^.*(".concat(available.join('|'), ").*$"), 'gm');
      var result = url.toUpperCase().replace(regex, '$1');

      if (result === '' || result === 'ERE') {
        return 'OUTROS';
      }

      if (result === 'RE/') {
        return 'RE';
      }

      return result;
    }
  })
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Blank.vue?vue&type=template&id=94b6e4ec&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Blank.vue?vue&type=template&id=94b6e4ec& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-container", { staticClass: "blank" })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Documents.vue?vue&type=template&id=a8180424&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Documents.vue?vue&type=template&id=a8180424& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "docs" },
    [
      _c(
        "portal",
        { attrs: { to: "content-before" } },
        [
          _c("OrsInnerToolbar", {
            attrs: { title: "Documentos", "title-icon": "mdi-file-document" }
          })
        ],
        1
      ),
      _vm.isLoading
        ? _c(
            "v-row",
            { staticClass: "mt-8", attrs: { justify: "center" } },
            [_c("v-progress-circular", { attrs: { indeterminate: "" } })],
            1
          )
        : _vm.noResults
        ? _c("v-row", { staticClass: "mt-8", attrs: { justify: "center" } }, [
            _c("h3", [_vm._v("\n      Nenhum documento encontrado.\n    ")])
          ])
        : [
            _c(
              "OrsTimeline",
              { attrs: { dense: _vm.$vuetify.breakpoint.smAndDown } },
              _vm._l(_vm.limitedPeriods, function(period, periodIdx) {
                return _c(
                  "OrsTimelineItem",
                  {
                    key: periodIdx,
                    attrs: { title: _vm.formatPeriod(period), color: "primary" }
                  },
                  [
                    _vm._l(period.arquivos, function(doc, docIdx) {
                      return _c(
                        "v-row",
                        { key: docIdx, staticClass: "docs-period" },
                        [
                          _c(
                            "v-col",
                            { attrs: { cols: "2" } },
                            [
                              _c(
                                "v-input",
                                {
                                  staticStyle: { "font-size": "10px" },
                                  attrs: { label: "Tipo" }
                                },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(_vm.getTipo(doc)) +
                                      "\n            "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _c(
                            "v-col",
                            { attrs: { cols: "10", sm: "4" } },
                            [
                              _c(
                                "v-input",
                                {
                                  staticStyle: { "font-size": "10px" },
                                  attrs: { label: "Nome do arquivo" }
                                },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(doc.nomeArquivo) +
                                      "\n            "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _c(
                            "v-col",
                            { attrs: { cols: "10", sm: "4" } },
                            [
                              _c(
                                "v-input",
                                {
                                  staticStyle: { "font-size": "11px" },
                                  attrs: { label: "Empresa" }
                                },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(doc.compArquivo) +
                                      "\n            "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _c(
                            "v-col",
                            {
                              staticClass: "text-center",
                              attrs: { cols: "10", sm: "2" }
                            },
                            [
                              _c(
                                "v-btn",
                                {
                                  staticStyle: { "font-size": "8px" },
                                  attrs: {
                                    outlined: "",
                                    rounded: "",
                                    small: "",
                                    href: doc.urlDownload,
                                    target: "_blank"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n              Download\n              "
                                  ),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "ml-1",
                                      attrs: { small: "" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-download\n              "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    }),
                    !period.show &&
                    period.ultimosArquivos &&
                    period.ultimosArquivos.length
                      ? _c(
                          "v-row",
                          [
                            _c(
                              "v-col",
                              { staticClass: "text-center" },
                              [
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { text: "", color: "primary" },
                                    on: {
                                      click: function($event) {
                                        period.show = true
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n              Ver mais\n            "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      : _vm._e(),
                    period.show
                      ? _c(
                          "div",
                          _vm._l(period.ultimosArquivos, function(doc, docIdx) {
                            return _c(
                              "v-row",
                              { key: docIdx, staticClass: "docs-period" },
                              [
                                _c(
                                  "v-col",
                                  { attrs: { cols: "2" } },
                                  [
                                    _c(
                                      "v-input",
                                      {
                                        staticStyle: { "font-size": "10px" },
                                        attrs: { label: "Tipo" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                " +
                                            _vm._s(_vm.getTipo(doc)) +
                                            "\n              "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                ),
                                _c(
                                  "v-col",
                                  { attrs: { cols: "10", sm: "4" } },
                                  [
                                    _c(
                                      "v-input",
                                      {
                                        staticStyle: { "font-size": "11px" },
                                        attrs: { label: "Nome do arquivo" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                " +
                                            _vm._s(doc.nomeArquivo) +
                                            "\n              "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                ),
                                _c(
                                  "v-col",
                                  { attrs: { cols: "10", sm: "4" } },
                                  [
                                    _c(
                                      "v-input",
                                      {
                                        staticStyle: { "font-size": "11px" },
                                        attrs: { label: "Empresa" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                " +
                                            _vm._s(doc.compArquivo) +
                                            "\n              "
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                ),
                                _c(
                                  "v-col",
                                  {
                                    staticClass: "text-center",
                                    attrs: { cols: "12", sm: "2" }
                                  },
                                  [
                                    _c(
                                      "v-btn",
                                      {
                                        staticStyle: { "font-size": "8px" },
                                        attrs: {
                                          outlined: "",
                                          rounded: "",
                                          small: "",
                                          href: doc.urlDownload,
                                          target: "_blank"
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                Download\n                "
                                        ),
                                        _c(
                                          "v-icon",
                                          {
                                            staticClass: "ml-1",
                                            attrs: { small: "" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                  mdi-download\n                "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          }),
                          1
                        )
                      : _vm._e()
                  ],
                  2
                )
              }),
              1
            ),
            !_vm.noMoreResults
              ? _c(
                  "div",
                  { staticClass: "d-flex justify-center ma-4" },
                  [
                    _c(
                      "v-btn",
                      {
                        attrs: {
                          color: "primary",
                          elevation: "0",
                          rounded: ""
                        },
                        on: { click: _vm.loadMorePeriods }
                      },
                      [_vm._v("\n        Carregar mais\n      ")]
                    )
                  ],
                  1
                )
              : _vm._e()
          ]
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./src/views/Blank.vue":
/*!*****************************!*\
  !*** ./src/views/Blank.vue ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Blank.vue?vue&type=template&id=94b6e4ec& */ "./src/views/Blank.vue?vue&type=template&id=94b6e4ec&");
/* harmony import */ var _Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Blank.vue?vue&type=script&lang=js& */ "./src/views/Blank.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_4__);





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */


_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VContainer"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/Blank.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/Blank.vue?vue&type=script&lang=js&":
/*!******************************************************!*\
  !*** ./src/views/Blank.vue?vue&type=script&lang=js& ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Blank.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Blank.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/Blank.vue?vue&type=template&id=94b6e4ec&":
/*!************************************************************!*\
  !*** ./src/views/Blank.vue?vue&type=template&id=94b6e4ec& ***!
  \************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Blank.vue?vue&type=template&id=94b6e4ec& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Blank.vue?vue&type=template&id=94b6e4ec&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Blank_vue_vue_type_template_id_94b6e4ec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/views/Documents.vue":
/*!*********************************!*\
  !*** ./src/views/Documents.vue ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Documents.vue?vue&type=template&id=a8180424& */ "./src/views/Documents.vue?vue&type=template&id=a8180424&");
/* harmony import */ var _Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Documents.vue?vue&type=script&lang=js& */ "./src/views/Documents.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_4__);





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */








_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCol: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VCol"],VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VContainer"],VIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VIcon"],VInput: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VInput"],VProgressCircular: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VProgressCircular"],VRow: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/Documents.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/Documents.vue?vue&type=script&lang=js&":
/*!**********************************************************!*\
  !*** ./src/views/Documents.vue?vue&type=script&lang=js& ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Documents.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Documents.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/Documents.vue?vue&type=template&id=a8180424&":
/*!****************************************************************!*\
  !*** ./src/views/Documents.vue?vue&type=template&id=a8180424& ***!
  \****************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Documents.vue?vue&type=template&id=a8180424& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Documents.vue?vue&type=template&id=a8180424&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Documents_vue_vue_type_template_id_a8180424___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=documents.js.map