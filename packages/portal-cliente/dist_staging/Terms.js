(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Terms"],{

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _OrsInnerToolbar = _interopRequireDefault(__webpack_require__(/*! @/components/OrsInnerToolbar.vue */ "./src/components/OrsInnerToolbar.vue"));

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default = {
  name: 'Terms',
  components: {
    OrsInnerToolbar: _OrsInnerToolbar.default
  },
  props: {
    redirect: {
      type: Object,
      required: true
    },
    onAccept: {
      type: Function,
      required: true
    }
  },
  methods: {
    back: function back() {
      this.$router.push(this.redirect);
    },
    accept: function accept() {
      this.onAccept();
      this.back();
    }
  }
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "terms" },
    [
      _c(
        "portal",
        { attrs: { to: "content-before" } },
        [
          _c("OrsInnerToolbar", {
            staticClass: "terms-title",
            attrs: { title: "Termo de aceite" }
          })
        ],
        1
      ),
      _c(
        "v-btn",
        {
          staticClass: "terms-back font-weight-bold",
          attrs: { text: "" },
          on: { click: _vm.back }
        },
        [
          _c("v-icon", { attrs: { left: "" } }, [
            _vm._v("\n      mdi-chevron-left\n    ")
          ]),
          _vm._v("\n    voltar\n  ")
        ],
        1
      ),
      _c("div", { staticClass: "terms-text" }, [
        _c("h2", [
          _vm._v("TERMO DE CONFIDENCIALIDADE E ACORDO DE COMPETÊNCIAS")
        ]),
        _c("p", [
          _vm._v(
            "\n      Pelo presente Termo de Confidencialidade e Acordo de Competências,\n      os Clientes da Orsegups Participações S.A., e de todas as suas\n      empresas coligadas, que assinam digitalmente este Instrumento,\n      passam a ter o direito de acessar a Plataforma de Gestão de\n      Pessoal (Meu Presença) e Documentos (Meus Documentos) da\n      Orsegups S.A.\n    "
          )
        ]),
        _c("h3", [_vm._v("Do Objetivo")]),
        _c("p", [
          _vm._v(
            "\n      Conceder pleno acesso aos Clientes da Orsegups Participações S.A.,\n      e de suas empresas coligadas, através do Portal do Cliente, em\n      todos os eventos, controles e processos registrados que lhe são\n      pertinentes, promovendo a transparência das ações na Prestação dos\n      Serviços, bem como definir as competências das partes neste\n      processo de automatização das informações.\n    "
          )
        ]),
        _c("h3", [_vm._v("Do Sigilo")]),
        _c("p", [
          _vm._v(
            "\n      O Cliente, os seus representantes, e/ou os seus colaboradores\n      autorizados, doravante usuários da Plataforma de Gestão da Orsegups,\n      comprometem-se através deste instrumento, e sob pena da lei, a zelar\n      e a guardar sigilo sobre todas as informações que terão acesso, a\n      saber: do(s) Contrato(s); das Ações de Segurança; dos Colaboradores\n      alocados na prestação dos serviços e correlatas.\n    "
          )
        ]),
        _c("h3", [_vm._v("Das Competências")]),
        _c("h4", [_vm._v("3.1. Da Orsegups")]),
        _c("p", [
          _vm._v(
            "\n      Compete exclusivamente a Orsegups Participações S.A., e as suas empresas coligadas:"
          ),
          _c("br"),
          _vm._v(
            "\n      a) Enviar por e-mail ao Cliente a(s) sua(as) Nota(s) Fiscal(is)\n      eletrônica(s) referente a prestação dos Serviços, seus\n      respectivos meios de pagamentos (boletos bancários) e as Guias\n      de Recolhimento (DARF e GPS), quando aplicável;"
          ),
          _c("br"),
          _vm._v(
            "\n      b) Manter a Plataforma de Gestão em pleno funcionamento com as\n      informações rotineiras de dados e eventos pertinentes a prestação\n      dos serviços ao Cliente, permitindo-lhe acesso a tal;"
          ),
          _c("br"),
          _vm._v(
            "\n      c) Disponibilizar, por até 6 meses, ao Cliente em sua conta\n      no Portal do Cliente Orsegups, de forma eletrônica, os documentos\n      administrativos, fiscais e contábeis por ele solicitados, que\n      permitam-lhe a auditoria contínua das rotinas e dos pagamentos\n      devidos e realizados pela Orsegups;"
          ),
          _c("br"),
          _vm._v(
            "\n      d) Manter o canal de comunicação do Cliente com a Orsegups,\n      através do gerenciamento de suas demandas e a pronta resposta."
          ),
          _c("br")
        ]),
        _c("h4", [_vm._v("3.2. Dos Clientes")]),
        _c("p", [
          _vm._v(
            "\n      Compete exclusivamente ao Cliente Promover a Auditoria da\n      Prestação do Serviços prestados pela Orsegups e a Auto Gestão\n      de suas demandas administrativas, contábeis e fiscais;\n      solicitações, sugestões e reclamações; pedidos e outras\n      necessidades; através de sua conta exclusiva no Portal do Cliente\n      Orsegups, inclusive a baixa, recebimento e providências que lhe\n      aprouver, por exclusiva via eletrônica, dos seguintes documentos:\n    "
          )
        ]),
        _c("p", [
          _vm._v(
            "\n      a) Nota Fiscal Eletrônica da prestação de serviços, emitida\n      mensalmente pela Orsegups,"
          ),
          _c("br"),
          _vm._v(
            "\n      b) Boleto Bancário (se esta for a forma de pagamento),"
          ),
          _c("br"),
          _vm._v("\n      c) Folha de Pagamento (quando aplicável),"),
          _c("br"),
          _vm._v(
            "\n      d) GRF (Guia de Recolhimento do FGTS, quando aplicável),"
          ),
          _c("br"),
          _vm._v(
            "\n      e) GPS (Guia da Previdência Social, quando aplicável),"
          ),
          _c("br"),
          _vm._v(
            "\n      f) Outros documentos específicos de cada Cliente, que até então\n      eram entregues de forma impressa, e que serão doravante\n      disponibilizados exclusivamente de forma eletrônica na conta do\n      Cliente, no Portal do Cliente Orsegups."
          ),
          _c("br")
        ]),
        _c("p", [
          _vm._v(
            "\n      E por concordarem com todas as premissas e resoluções constantes\n      neste instrumento, as partes aprovam e anuem ao expresso de forma\n      eletrônica.\n    "
          )
        ])
      ]),
      _c(
        "div",
        { staticClass: "terms-actions" },
        [
          _c(
            "v-btn",
            {
              staticClass: "font-weight-bold",
              attrs: { text: "", large: "" },
              on: { click: _vm.back }
            },
            [_vm._v("\n      Cancelar\n    ")]
          ),
          _c(
            "v-btn",
            {
              staticClass: "ml-4 font-weight-bold",
              attrs: { large: "", rounded: "", color: "primary" },
              on: { click: _vm.accept }
            },
            [_vm._v("\n      Aceito\n    ")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/css-loader??ref--6-oneOf-1-1!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/stylePostLoader.js!/builds/incuca/clientes/orsegups/node_modules/postcss-loader/src??ref--6-oneOf-1-2!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "../../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.terms-title .container {\n  max-width: 728px;\n  margin-left: auto;\n  margin-right: auto;\n}\n", ""]);

// exports


/***/ }),

/***/ "../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/css-loader??ref--6-oneOf-1-1!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/stylePostLoader.js!/builds/incuca/clientes/orsegups/node_modules/postcss-loader/src??ref--6-oneOf-1-2!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "../../node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-oneOf-1-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-oneOf-1-2!@/style/variables.css */ "../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!./src/style/variables.css?513c"), "");

// module
exports.push([module.i, "\n.terms[data-v-655c1786] {\n  line-height: 2;\n  font-family: 'Raleway', sans-serif;\n  max-width: 728px;\n  margin-left: auto;\n  margin-right: auto;\n}\n.terms .terms-back[data-v-655c1786] {\n  color: #de7c00;\n  color: var(--dash-color-3);\n}\n.terms .terms-actions[data-v-655c1786] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\nh2[data-v-655c1786] {\n  font-family: 'Encode Sans', sans-serif;\n  font-size: 18px;\n}\nh2[data-v-655c1786], h3[data-v-655c1786], h4[data-v-655c1786] {\n  color: #1E398D;\n  color: var(--primary-color-3);\n  font-weight: bold;\n}\nh3[data-v-655c1786] {\n  font-size: 16px;\n}\nh4[data-v-655c1786], p[data-v-655c1786] {\n  font-size: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ "../../node_modules/vue-style-loader/index.js?!../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/vue-style-loader??ref--6-oneOf-1-0!/builds/incuca/clientes/orsegups/node_modules/css-loader??ref--6-oneOf-1-1!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/stylePostLoader.js!/builds/incuca/clientes/orsegups/node_modules/postcss-loader/src??ref--6-oneOf-1-2!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-oneOf-1-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-oneOf-1-2!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=style&index=0&lang=css& */ "../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=0&lang=css&");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "../../node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("03aee66b", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "../../node_modules/vue-style-loader/index.js?!../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/vue-style-loader??ref--6-oneOf-1-0!/builds/incuca/clientes/orsegups/node_modules/css-loader??ref--6-oneOf-1-1!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/stylePostLoader.js!/builds/incuca/clientes/orsegups/node_modules/postcss-loader/src??ref--6-oneOf-1-2!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-oneOf-1-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-oneOf-1-2!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& */ "../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! ../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "../../node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("65e143ce", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./src/views/Terms.vue":
/*!*****************************!*\
  !*** ./src/views/Terms.vue ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Terms.vue?vue&type=template&id=655c1786&scoped=true& */ "./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true&");
/* harmony import */ var _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Terms.vue?vue&type=script&lang=js& */ "./src/views/Terms.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Terms.vue?vue&type=style&index=0&lang=css& */ "./src/views/Terms.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& */ "./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_6__);







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "655c1786",
  null
  
)

/* vuetify-loader */




_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_5___default()(component, {VBtn: vuetify_lib__WEBPACK_IMPORTED_MODULE_6__["VBtn"],VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_6__["VContainer"],VIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_6__["VIcon"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/Terms.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/Terms.vue?vue&type=script&lang=js&":
/*!******************************************************!*\
  !*** ./src/views/Terms.vue?vue&type=script&lang=js& ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/Terms.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************!*\
  !*** ./src/views/Terms.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader??ref--6-oneOf-1-0!../../../../node_modules/css-loader??ref--6-oneOf-1-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-oneOf-1-2!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=style&index=0&lang=css& */ "../../node_modules/vue-style-loader/index.js?!../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&":
/*!**************************************************************************************!*\
  !*** ./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader??ref--6-oneOf-1-0!../../../../node_modules/css-loader??ref--6-oneOf-1-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-oneOf-1-2!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css& */ "../../node_modules/vue-style-loader/index.js?!../../node_modules/css-loader/index.js?!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src/index.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=style&index=1&id=655c1786&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_style_index_1_id_655c1786_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true&":
/*!************************************************************************!*\
  !*** ./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=template&id=655c1786&scoped=true& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/Terms.vue?vue&type=template&id=655c1786&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_655c1786_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=Terms.js.map