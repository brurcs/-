(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["AlarmsList"],{

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsList.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/AlarmsList.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

__webpack_require__(/*! core-js/modules/web.dom.iterable */ "../../node_modules/core-js/modules/web.dom.iterable.js");

__webpack_require__(/*! core-js/modules/es6.object.keys */ "../../node_modules/core-js/modules/es6.object.keys.js");

var _defineProperty2 = _interopRequireDefault(__webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js"));

__webpack_require__(/*! core-js/modules/es6.function.name */ "../../node_modules/core-js/modules/es6.function.name.js");

var _objectSpread2 = _interopRequireDefault(__webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js"));

var _vuex = __webpack_require__(/*! vuex */ "../../node_modules/vuex/dist/vuex.esm.js");

var _OrsInnerToolbar = _interopRequireDefault(__webpack_require__(/*! @/components/OrsInnerToolbar.vue */ "./src/components/OrsInnerToolbar.vue"));

var _OrsCard = _interopRequireDefault(__webpack_require__(/*! @/components/OrsCard.vue */ "./src/components/OrsCard.vue"));

var _joinFields = _interopRequireDefault(__webpack_require__(/*! @/lib/joinFields */ "./src/lib/joinFields.js"));

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var alarmsList = (0, _vuex.createNamespacedHelpers)('views/alarmsList');
var session = (0, _vuex.createNamespacedHelpers)('session');
var _default = {
  name: 'AlarmsList',
  components: {
    OrsInnerToolbar: _OrsInnerToolbar.default,
    OrsCard: _OrsCard.default
  },
  computed: (0, _objectSpread2.default)({}, alarmsList.mapGetters(['isLoading', 'noResults']), alarmsList.mapState(['alarms']), {
    isContainerEnabled: function isContainerEnabled() {
      var map = {
        AlarmsList: true,
        AlarmsServiceOrders: false
      };
      return map[this.$route.name];
    }
  }),
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.loadAlarms();
      vm.addAccountChangeListener(vm.loadAlarms);
    });
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    this.removeAccountChangeListener(this.loadAlarms);
    next();
  },
  methods: (0, _objectSpread2.default)({}, session.mapMutations(['addAccountChangeListener', 'removeAccountChangeListener']), alarmsList.mapActions(['loadAlarms']), {
    getAddress: function getAddress(alarm) {
      return (0, _joinFields.default)(['fantasia', 'endereco', 'bairro', 'cidade', 'uf'], {
        from: alarm,
        transform: function transform(f) {
          return f.toLowerCase();
        }
      });
    },
    onAlarmEventClick: function onAlarmEventClick(alarm) {
      var linkEventos = alarm.linkEventos;
      var url = new URL(linkEventos);

      var getEndpoint = function getEndpoint() {
        return "".concat(url.protocol, "//").concat(url.host).concat(url.pathname);
      };

      var getField = function getField(field) {
        return (0, _defineProperty2.default)({}, field, url.searchParams.get(field));
      };

      var fields = (0, _objectSpread2.default)({}, getField('central'), getField('senha'), getField('cliente'));
      var form = document.createElement('form');
      form.action = getEndpoint();
      form.method = 'post';
      form.target = '_blank';
      form.style.display = 'none';
      var inputs = Object.keys(fields).map(function (fieldName) {
        var input = document.createElement('input');
        input.name = fieldName;
        input.type = 'hidden';
        input.value = fields[fieldName];
        return input;
      });
      var submit = document.createElement('input');
      submit.type = 'submit';
      submit.value = 'Eventos';
      inputs.push(submit);
      inputs.forEach(function (i) {
        return form.appendChild(i);
      });
      this.$el.appendChild(form);
      submit.click();
      this.$el.removeChild(form);
    }
  })
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.isContainerEnabled
    ? _c(
        "v-container",
        [
          _c(
            "portal",
            { attrs: { to: "content-before" } },
            [
              _c("OrsInnerToolbar", {
                attrs: {
                  title: "Segurança Eletrônica",
                  "title-icon": "mdi-shield-check"
                }
              })
            ],
            1
          ),
          _vm.isLoading
            ? _c(
                "v-row",
                { staticClass: "mt-8", attrs: { justify: "center" } },
                [_c("v-progress-circular", { attrs: { indeterminate: "" } })],
                1
              )
            : _vm.noResults
            ? _c(
                "v-row",
                { staticClass: "mt-8", attrs: { justify: "center" } },
                [_c("h3", [_vm._v("\n      Nenhum alarme para exibir\n    ")])]
              )
            : _c(
                "v-row",
                _vm._l(_vm.alarms, function(alarm, alarmIdx) {
                  return _c(
                    "v-col",
                    { key: alarmIdx, attrs: { xs: "12", sm: "6", md: "4" } },
                    [
                      _c(
                        "OrsCard",
                        {
                          attrs: { title: alarm.tipoConta },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "footer",
                                fn: function() {
                                  return [
                                    _c(
                                      "v-btn",
                                      {
                                        attrs: { text: "", small: "" },
                                        on: {
                                          click: function($event) {
                                            return _vm.onAlarmEventClick(alarm)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n            Evento\n          "
                                        )
                                      ]
                                    ),
                                    _c(
                                      "v-btn",
                                      {
                                        attrs: {
                                          rounded: "",
                                          outlined: "",
                                          small: "",
                                          to: {
                                            name: "AlarmsServiceOrders",
                                            params: { alarmIdx: alarmIdx }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n            Ordem de serviço\n          "
                                        )
                                      ]
                                    )
                                  ]
                                },
                                proxy: true
                              }
                            ],
                            null,
                            true
                          )
                        },
                        [
                          _c(
                            "v-list",
                            { attrs: { dense: "" } },
                            [
                              _c(
                                "v-list-item",
                                [
                                  _c(
                                    "v-list-item-icon",
                                    [
                                      _c(
                                        "v-icon",
                                        { attrs: { color: "primary" } },
                                        [
                                          _vm._v(
                                            "\n                mdi-map-marker\n              "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _c(
                                    "v-list-item-content",
                                    [
                                      _c(
                                        "v-list-item-title",
                                        { staticClass: "text-capitalize" },
                                        [
                                          _vm._v(
                                            "\n                " +
                                              _vm._s(_vm.getAddress(alarm)) +
                                              "\n              "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _c(
                                "v-list-item",
                                [
                                  _c(
                                    "v-list-item-icon",
                                    [
                                      _c(
                                        "v-icon",
                                        { attrs: { color: "primary" } },
                                        [
                                          _vm._v(
                                            "\n                mdi-account\n              "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _c(
                                    "v-list-item-content",
                                    [
                                      _c("v-list-item-title", [
                                        _vm._v(
                                          "\n                " +
                                            _vm._s(alarm.centralParticao) +
                                            "\n              "
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                }),
                1
              )
        ],
        1
      )
    : _c("router-view")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./src/views/AlarmsList.vue":
/*!**********************************!*\
  !*** ./src/views/AlarmsList.vue ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AlarmsList.vue?vue&type=template&id=bdf9948c& */ "./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c&");
/* harmony import */ var _AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AlarmsList.vue?vue&type=script&lang=js& */ "./src/views/AlarmsList.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_4__);





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */












_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCol: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VCol"],VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VContainer"],VIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VIcon"],VList: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VList"],VListItem: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VListItem"],VListItemContent: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VListItemContent"],VListItemIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VListItemIcon"],VListItemTitle: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VListItemTitle"],VProgressCircular: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VProgressCircular"],VRow: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/AlarmsList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/AlarmsList.vue?vue&type=script&lang=js&":
/*!***********************************************************!*\
  !*** ./src/views/AlarmsList.vue?vue&type=script&lang=js& ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AlarmsList.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsList.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c&":
/*!*****************************************************************!*\
  !*** ./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c& ***!
  \*****************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AlarmsList.vue?vue&type=template&id=bdf9948c& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsList.vue?vue&type=template&id=bdf9948c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsList_vue_vue_type_template_id_bdf9948c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=AlarmsList.js.map