(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ContactsView"],{

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/ContactsView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/ContactsView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(__webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js"));

__webpack_require__(/*! core-js/modules/es6.number.constructor */ "../../node_modules/core-js/modules/es6.number.constructor.js");

var _vuex = __webpack_require__(/*! vuex */ "../../node_modules/vuex/dist/vuex.esm.js");

var _OrsInnerToolbar = _interopRequireDefault(__webpack_require__(/*! @/components/OrsInnerToolbar.vue */ "./src/components/OrsInnerToolbar.vue"));

var _OrsTimeline = _interopRequireDefault(__webpack_require__(/*! ../components/OrsTimeline.vue */ "./src/components/OrsTimeline.vue"));

var _OrsTimelineItem = _interopRequireDefault(__webpack_require__(/*! ../components/OrsTimelineItem.vue */ "./src/components/OrsTimelineItem.vue"));

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default = {
  name: 'ContactsView',
  components: {
    OrsInnerToolbar: _OrsInnerToolbar.default,
    OrsTimeline: _OrsTimeline.default,
    OrsTimelineItem: _OrsTimelineItem.default
  },
  props: {
    id: {
      type: [String, Number],
      required: true
    }
  },
  computed: (0, _vuex.mapState)('views/contactsList', ['contact', 'contacts']),
  watch: {
    contacts: function contacts() {
      // FIXME: not so beaulty but works, loadContact when parent contacts changes
      this.loadContact(this.id);
    }
  },
  methods: (0, _objectSpread2.default)({}, (0, _vuex.mapActions)('views/contactsList', ['loadContact']), {
    getContactColor: function getContactColor(contact) {
      var satisfacao = contact.satisfacao;
      var map = {
        Satisfeito: 'teal',
        Insatisfeito: 'red',
        'Sem Contato': 'orange',
        'Sem contato': 'orange'
      };
      return satisfacao ? map[satisfacao] : 'primary';
    }
  })
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/ContactsView.vue?vue&type=template&id=31f512d2&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/ContactsView.vue?vue&type=template&id=31f512d2& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "contView" },
    [
      _vm.contact
        ? [
            _c(
              "portal",
              { attrs: { to: "content-before" } },
              [
                _c("OrsInnerToolbar", {
                  attrs: {
                    title: "Histórico solicitação " + _vm.contact.codigoTarefa
                  }
                })
              ],
              1
            ),
            _c(
              "v-row",
              { attrs: { align: "center" } },
              [
                _c(
                  "v-col",
                  { attrs: { cols: "4" } },
                  [
                    _c(
                      "v-btn",
                      {
                        staticClass: "font-weight-bold",
                        attrs: {
                          text: "",
                          "x-large": "",
                          color: "primary",
                          to: { name: "ContactsList" }
                        }
                      },
                      [
                        _c("v-icon", { attrs: { left: "" } }, [
                          _vm._v("\n            mdi-chevron-left\n          ")
                        ]),
                        _vm._v("\n          Voltar\n        ")
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "12" } },
                  [
                    _c(
                      "OrsTimeline",
                      { attrs: { dense: _vm.$vuetify.breakpoint.smAndDown } },
                      _vm._l(_vm.contact.registroAtividades, function(
                        activity,
                        activityIdx
                      ) {
                        return _c("OrsTimelineItem", {
                          key: activityIdx,
                          attrs: { color: _vm.getContactColor(_vm.contact) },
                          scopedSlots: _vm._u(
                            [
                              {
                                key: "default",
                                fn: function() {
                                  return [
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "12" } },
                                          [
                                            _c(
                                              "v-input",
                                              { attrs: { label: "Atividade" } },
                                              [
                                                _vm._v(
                                                  "\n                    " +
                                                    _vm._s(
                                                      activity.nomeAtividade
                                                    ) +
                                                    "\n                  "
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "12", md: "6" } },
                                          [
                                            _c(
                                              "v-input",
                                              {
                                                attrs: {
                                                  label: "Data de início"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                    " +
                                                    _vm._s(
                                                      activity.dataInicialString
                                                    ) +
                                                    "\n                  "
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "12", md: "6" } },
                                          [
                                            _c(
                                              "v-input",
                                              { attrs: { label: "Prazo" } },
                                              [
                                                _vm._v(
                                                  "\n                    " +
                                                    _vm._s(
                                                      activity.prazoString
                                                    ) +
                                                    "\n                  "
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "12" } },
                                          [
                                            _c(
                                              "v-input",
                                              {
                                                attrs: { label: "Responsável" }
                                              },
                                              [
                                                _c("div", {
                                                  domProps: {
                                                    innerHTML: _vm._s(
                                                      activity.responsavel
                                                    )
                                                  }
                                                })
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "12" } },
                                          [
                                            _c(
                                              "v-input",
                                              { attrs: { label: "Contato" } },
                                              [
                                                _c("div", {
                                                  domProps: {
                                                    innerHTML: _vm._s(
                                                      activity.descricao
                                                    )
                                                  }
                                                })
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                },
                                proxy: true
                              }
                            ],
                            null,
                            true
                          )
                        })
                      }),
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]
        : _c(
            "v-row",
            { attrs: { justify: "center" } },
            [_c("v-progress-circular", { attrs: { indeterminate: "" } })],
            1
          )
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./src/views/ContactsView.vue":
/*!************************************!*\
  !*** ./src/views/ContactsView.vue ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ContactsView.vue?vue&type=template&id=31f512d2& */ "./src/views/ContactsView.vue?vue&type=template&id=31f512d2&");
/* harmony import */ var _ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ContactsView.vue?vue&type=script&lang=js& */ "./src/views/ContactsView.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_4__);





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */








_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCol: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VCol"],VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VContainer"],VIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VIcon"],VInput: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VInput"],VProgressCircular: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VProgressCircular"],VRow: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/ContactsView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/ContactsView.vue?vue&type=script&lang=js&":
/*!*************************************************************!*\
  !*** ./src/views/ContactsView.vue?vue&type=script&lang=js& ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ContactsView.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/ContactsView.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/ContactsView.vue?vue&type=template&id=31f512d2&":
/*!*******************************************************************!*\
  !*** ./src/views/ContactsView.vue?vue&type=template&id=31f512d2& ***!
  \*******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ContactsView.vue?vue&type=template&id=31f512d2& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/ContactsView.vue?vue&type=template&id=31f512d2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactsView_vue_vue_type_template_id_31f512d2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=ContactsView.js.map