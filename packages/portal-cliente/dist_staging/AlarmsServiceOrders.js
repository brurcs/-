(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["AlarmsServiceOrders"],{

/***/ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--12-0!/builds/incuca/clientes/orsegups/node_modules/babel-loader/lib!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime/helpers/interopRequireDefault */ "../../node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(__webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread */ "../../node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js"));

__webpack_require__(/*! core-js/modules/es6.number.constructor */ "../../node_modules/core-js/modules/es6.number.constructor.js");

var _vuex = __webpack_require__(/*! vuex */ "../../node_modules/vuex/dist/vuex.esm.js");

var _OrsInnerToolbar = _interopRequireDefault(__webpack_require__(/*! @/components/OrsInnerToolbar.vue */ "./src/components/OrsInnerToolbar.vue"));

var _OrsTimeline = _interopRequireDefault(__webpack_require__(/*! ../components/OrsTimeline.vue */ "./src/components/OrsTimeline.vue"));

var _OrsTimelineItem = _interopRequireDefault(__webpack_require__(/*! @/components/OrsTimelineItem.vue */ "./src/components/OrsTimelineItem.vue"));

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var alarmsList = (0, _vuex.createNamespacedHelpers)('views/alarmsList');
var _default = {
  name: 'AlarmsServiceOrder',
  components: {
    OrsInnerToolbar: _OrsInnerToolbar.default,
    OrsTimeline: _OrsTimeline.default,
    OrsTimelineItem: _OrsTimelineItem.default
  },
  props: {
    alarmIdx: {
      type: [Number, String],
      required: true
    }
  },
  computed: (0, _objectSpread2.default)({}, alarmsList.mapGetters(['isLoading', 'noResults']), alarmsList.mapState(['alarms', 'serviceOrders'])),
  watch: {
    alarms: function alarms() {
      // FIXME: not so beaulty but works, loadContact when parent alarms changes
      this.loadServiceOrder(this.alarmIdx);
    }
  },
  methods: (0, _objectSpread2.default)({}, alarmsList.mapActions(['loadServiceOrder']), {
    getOrderColor: function getOrderColor(order) {
      var map = {
        1: 'teal',
        0: 'primary'
      };
      return map[Number(order.fechado)];
    },
    getOrderSituation: function getOrderSituation(order) {
      var map = {
        1: 'Fechada',
        0: 'Aberta'
      };
      return map[Number(order.fechado)] || 'N/A';
    }
  })
};
exports.default = _default;

/***/ }),

/***/ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!/builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/loader.js!/builds/incuca/clientes/orsegups/node_modules/cache-loader/dist/cjs.js??ref--0-0!/builds/incuca/clientes/orsegups/node_modules/vue-loader/lib??vue-loader-options!./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "portal",
        { attrs: { to: "content-before" } },
        [_c("OrsInnerToolbar", { attrs: { title: "Ordem de serviço" } })],
        1
      ),
      _c(
        "v-btn",
        {
          staticClass: "font-weight-bold",
          attrs: {
            text: "",
            "x-large": "",
            color: "primary",
            to: { name: "AlarmsList" }
          }
        },
        [
          _c("v-icon", { attrs: { left: "" } }, [
            _vm._v("\n      mdi-chevron-left\n    ")
          ]),
          _vm._v("\n    Voltar\n  ")
        ],
        1
      ),
      _vm.isLoading
        ? _c(
            "v-row",
            { staticClass: "mt-8", attrs: { justify: "center" } },
            [_c("v-progress-circular", { attrs: { indeterminate: "" } })],
            1
          )
        : _vm.noResults || _vm.serviceOrders === null
        ? _c("v-row", { staticClass: "mt-8", attrs: { justify: "center" } }, [
            _c("h3", [_vm._v("\n      Não existem ordens de serviço\n    ")])
          ])
        : _c(
            "OrsTimeline",
            { attrs: { dense: _vm.$vuetify.breakpoint.smAndDown } },
            _vm._l(_vm.serviceOrders, function(order, orderIdx) {
              return _c(
                "OrsTimelineItem",
                { key: orderIdx, attrs: { color: _vm.getOrderColor(order) } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", md: "3" } },
                        [
                          _c("v-input", { attrs: { label: "OS" } }, [
                            _vm._v(
                              "\n            " +
                                _vm._s(order.idOrdem) +
                                "\n          "
                            )
                          ])
                        ],
                        1
                      ),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", md: "3" } },
                        [
                          _c("v-input", { attrs: { label: "Abertura" } }, [
                            _vm._v(
                              "\n            " +
                                _vm._s(order.abertura) +
                                "\n          "
                            )
                          ])
                        ],
                        1
                      ),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", md: "3" } },
                        [
                          _c("v-input", { attrs: { label: "Fechamento" } }, [
                            _vm._v(
                              "\n            " +
                                _vm._s(order.fechamento || "--") +
                                "\n          "
                            )
                          ])
                        ],
                        1
                      ),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", md: "3" } },
                        [
                          _c(
                            "v-input",
                            {
                              class: _vm.getOrderColor(order) + "--text",
                              attrs: { label: "Situação" }
                            },
                            [
                              _vm._v(
                                "\n            " +
                                  _vm._s(_vm.getOrderSituation(order)) +
                                  "\n          "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        [
                          _c("v-input", { attrs: { label: "Descrição" } }, [
                            _vm._v(
                              "\n            " +
                                _vm._s(order.defeito) +
                                "\n          "
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            }),
            1
          )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./src/views/AlarmsServiceOrders.vue":
/*!*******************************************!*\
  !*** ./src/views/AlarmsServiceOrders.vue ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4& */ "./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4&");
/* harmony import */ var _AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AlarmsServiceOrders.vue?vue&type=script&lang=js& */ "./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "../../node_modules/vue-loader/lib/runtime/componentNormalizer.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /builds/incuca/clientes/orsegups/node_modules/vuetify-loader/lib/runtime/installComponents.js */ "../../node_modules/vuetify-loader/lib/runtime/installComponents.js");
/* harmony import */ var _builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuetify/lib */ "../../node_modules/vuetify/lib/index.js");
/* harmony import */ var vuetify_lib__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuetify_lib__WEBPACK_IMPORTED_MODULE_4__);





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* vuetify-loader */








_builds_incuca_clientes_orsegups_node_modules_vuetify_loader_lib_runtime_installComponents_js__WEBPACK_IMPORTED_MODULE_3___default()(component, {VBtn: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VBtn"],VCol: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VCol"],VContainer: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VContainer"],VIcon: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VIcon"],VInput: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VInput"],VProgressCircular: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VProgressCircular"],VRow: vuetify_lib__WEBPACK_IMPORTED_MODULE_4__["VRow"]})


/* hot reload */
if (false) { var api; }
component.options.__file = "src/views/AlarmsServiceOrders.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../../../node_modules/babel-loader/lib!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AlarmsServiceOrders.vue?vue&type=script&lang=js& */ "../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/babel-loader/lib/index.js!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsServiceOrders.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4&":
/*!**************************************************************************!*\
  !*** ./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!cache-loader?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"f8f1581e-vue-loader-template"}!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vuetify-loader/lib/loader.js!../../../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4& */ "../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"f8f1581e-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js?!../../node_modules/vuetify-loader/lib/loader.js!../../node_modules/cache-loader/dist/cjs.js?!../../node_modules/vue-loader/lib/index.js?!./src/views/AlarmsServiceOrders.vue?vue&type=template&id=0ffa7ae4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _cache_loader_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_f8f1581e_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vuetify_loader_lib_loader_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AlarmsServiceOrders_vue_vue_type_template_id_0ffa7ae4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=AlarmsServiceOrders.js.map