/* eslint-disable import/no-extraneous-dependencies */

/* eslint-env node */
const ON_DEATH = require('death');
const { spawn } = require('child_process');
const console = require('console');


let lastMessage = new Date();
const pids = [];
const spawnCy = () => {
  const localCy = spawn('node', [
    './node_modules/.bin/vue-cli-service',
    'test:e2e',
    '--headless',
    '--mode',
    `${process.env.NODE_ENV || 'production'}`], {
    env: {
      ...process.env,
      FORCE_COLOR: true,
    },
  });

  localCy.stdout.on('data', (data) => {
    lastMessage = new Date();
    process.stdout.write(data.toString());
  });

  localCy.stderr.on('data', (data) => {
    lastMessage = new Date();
    process.stderr.write(data.toString());
  });

  localCy.on('close', (code) => {
    process.exit(code);
  });
  return localCy;
};

let cy = spawnCy();

pids.push(cy.pid);

let numRetries = 0;

const end = () => {
  const { pid } = cy;
  process.kill(pid, 'SIGKILL');
  console.error(`Killed pid ${pid}`);

  // looks for X of Y failed in the test summary, if finds, exits with 1
  // otherwise exits with 0.
  // process.exit(/\d+\s+of\s+\d+\s+failed/.test(out) ? 1 : 0);
};

const TIMEOUT_FROM_LAST_MESSAGE_IN_SEC = 3 * 60;

const checkLastMessage = () => {
  const diffInSeconds = (new Date() - lastMessage) / 1000;
  console.log(`Last time we saw a message was ${diffInSeconds} seconds ago`);
  if (diffInSeconds > TIMEOUT_FROM_LAST_MESSAGE_IN_SEC) {
    console.log('The last time we saw a message was greater than 3 minutes ago, killing');
    end(cy.pid);
    console.info(`Retries: ${numRetries}`);
    if (numRetries < 4) {
      cy = spawnCy();
      pids.push(cy.pid);
      numRetries += 1;
    } else {
      console.error('Retried 4 times to re-run cypress and still couldnt get it to not get stuck');
      process.exit(1);
    }
  }
  setTimeout(checkLastMessage, 30000);
};

setTimeout(checkLastMessage, 30000);


ON_DEATH(() => {
  pids.forEach((pid) => {
    try {
      console.info(`On death cleaning up if we got stuck along the way. ${pid}`);
      process.kill(pid);
    } catch (err) {
      console.error(`Caught error with killing pid ${pid}. Probably already killed.`);
    }
  });
});
