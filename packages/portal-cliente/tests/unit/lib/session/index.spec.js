import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { createLocalVue, mount } from '@vue/test-utils';
import localforage from 'localforage';
import createSession from '@/lib/session';
import sessionModule from '@/lib/session/session.module';

jest.mock('localforage');

function fakeMount(localVue, router) {
  const FakeComponent = {
    template: '<router-view />',
  };
  return mount(FakeComponent, {
    localVue,
    router,
  });
}

function loadRouter(options, localVue = createLocalVue()) {
  localVue.use(VueRouter);
  const router = new VueRouter(options);
  router.pushAsync = route => new Promise((resolve, reject) => {
    router.push(route, resolve, reject);
  });
  return router;
}

function loadVuex(plugins, localVue = createLocalVue()) {
  localVue.use(Vuex);
  return new Vuex.Store({ plugins });
}

describe('session lib', () => {
  const feathersClient = {
    hooks: jest.fn(),
  };

  it('returns a vuex plugin', () => {
    const router = new VueRouter();
    const plugin = createSession(router, feathersClient, 'foo');
    expect(plugin).toBeInstanceOf(Function);
    const store = loadVuex([plugin]);
    expect(store).toBeDefined();
  });

  describe('vuex plugin', () => {
    it('register session module on store', () => {
      const router = {
        beforeEach: jest.fn(),
      };
      const plugin = createSession(router, feathersClient, 'foo');
      const store = {
        registerModule: jest.fn(),
      };
      plugin(store);
      expect(store.registerModule).toBeCalledWith(
        'session',
        sessionModule,
      );
    });

    it('attach a route guard', () => {
      const router = {
        beforeEach: jest.fn(),
      };
      const plugin = createSession(router, feathersClient, 'foo');
      loadVuex([plugin]);
      expect(router.beforeEach).toBeCalledWith(
        expect.any(Function),
      );
    });

    describe('route guard', () => {
      beforeEach(() => {
        localforage.clear();
      });

      it('skip if route does not requires auth', (done) => {
        const localVue = createLocalVue();
        const router = loadRouter({
          routes: [
            {
              path: '/',
              name: 'Home',
              component: {
                render: h => h('div'),
                beforeRouteEnter(to, from, next) {
                  next((vm) => {
                    expect(vm.$route.name).toEqual('Home');
                    done();
                  });
                },
              },
            },
          ],
        }, localVue);
        const plugin = createSession(router, feathersClient);
        loadVuex([plugin], localVue);
        fakeMount(localVue, router);
      });

      it('load user accounts if route requires auth but user is loggedIn', (done) => {
        const fakeAccount = {
          id: 1,
          contractCode: '001',
        };
        const fakeUser = {
          id: 1,
        };
        const fakeAccountsFind = jest.fn(() => Promise.resolve([fakeAccount]));
        const fakeUsersGet = jest.fn(() => Promise.resolve([fakeUser]));
        const localVue = createLocalVue();
        const router = loadRouter({
          routes: [
            {
              path: '/',
              name: 'Home',
              meta: { requiresAuth: true },
              component: {
                render: h => h('div'),
                beforeRouteEnter(to, from, next) {
                  next((vm) => {
                    expect(vm.$route.name).toEqual('Home');
                    // if fakeAccountsFind has been called
                    // it proves that loadUserAccountshas been called
                    expect(fakeAccountsFind).toBeCalled();
                    done();
                  });
                },
              },
            },
          ],
        }, localVue);
        const plugin = createSession(router, feathersClient);
        const fakeAccountsPlugin = (store) => {
          store.registerModule('accounts', {
            namespaced: true,
            actions: {
              find: fakeAccountsFind,
            },
          });
        };
        const fakeUsersPlugin = (store) => {
          store.registerModule('users', {
            namespaced: true,
            actions: {
              get: fakeUsersGet,
            },
          });
        };
        loadVuex([plugin, fakeAccountsPlugin, fakeUsersPlugin], localVue);
        const token = {
          id: 1,
          accessToken: 'foo',
          expiresIn: 9999,
          createdAt: new Date(),
        };
        localforage.getItem.mockResolvedValue(token);
        fakeMount(localVue, router);
      });

      it('redirect if route requires auth and token is expired', (done) => {
        const localVue = createLocalVue();
        const router = loadRouter({
          routes: [
            {
              path: '/',
              name: 'Home',
              meta: { requiresAuth: true },
              component: {
                render: h => h('div'),
              },
            },
            {
              path: '/',
              name: 'Login',
              component: {
                render: h => h('div'),
                beforeRouteEnter(to, from, next) {
                  next((vm) => {
                    expect(vm.$route.name).toEqual('Login');
                    done();
                  });
                },
              },
            },
          ],
        }, localVue);
        const plugin = createSession(router, feathersClient);
        loadVuex([plugin], localVue);
        const token = {
          id: 1,
          accessToken: 'foo',
          expiresIn: 9999,
          createdAt: new Date('1999-01-01'),
        };
        localforage.getItem.mockResolvedValue(token);
        fakeMount(localVue, router);
      });

      it('redirect if route requires auth and there is no token', (done) => {
        const localVue = createLocalVue();
        const router = loadRouter({
          routes: [
            {
              path: '/',
              name: 'Home',
              meta: { requiresAuth: true },
              component: {
                render: h => h('div'),
              },
            },
            {
              path: '/',
              name: 'Login',
              component: {
                render: h => h('div'),
                beforeRouteEnter(to, from, next) {
                  next((vm) => {
                    expect(vm.$route.name).toEqual('Login');
                    expect(vm.$route.query).toEqual({
                      redirect: '/?redirect=%2F',
                    });
                    done();
                  });
                },
              },
            },
          ],
        }, localVue);
        const plugin = createSession(router, feathersClient);
        loadVuex([plugin], localVue);
        fakeMount(localVue, router);
      });
    });
  });
});
