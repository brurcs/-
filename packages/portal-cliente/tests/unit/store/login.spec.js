import loginModule from '@/store/login';

describe('login store', () => {
  const {
    login, logout, passwordReset, checkUserFirstLogin,
    changeUserPassword,
  } = loginModule.actions;

  describe('state', () => {
    it('has logoutCallback state', () => {
      expect(loginModule.state).toMatchObject({
        logoutCallback: expect.any(Function),
      });
      loginModule.state.logoutCallback();
      expect(window.location.pathname).toEqual('/');
    });
  });

  describe('mutations', () => {
    it('mutates logoutCallback', () => {
      const fakeState = {
        logoutCallback: () => {},
      };
      const cb = jest.fn();
      loginModule.mutations.setLogoutCallback(fakeState, cb);
      expect(fakeState.logoutCallback).toEqual(cb);
    });
  });

  describe('getters', () => {
    it('accounts returns user accounts', () => {
      const userAccounts = [];
      const state = { userAccounts };
      expect(loginModule.getters.accounts(state)).toBe(userAccounts);
    });

    it('returns current user id if token is set', () => {
      const { currentUserId } = loginModule.getters;
      const st = {};
      const gt = {};
      const userId = 1;
      const rootState = {
        session: {
          token: {
            userId,
          },
        },
      };
      expect(currentUserId(st, gt, rootState)).toBe(userId);
    });
  });

  describe('actions', () => {
    describe('login action', () => {
      it('call onSuccess and create session', (done) => {
        const username = 'producao@incuca.com.br';
        const password = 'bc3c6e15';
        const token = { id: 1, access_token: 'foobar' };
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.resolve([token])),
        };
        const thisArg = {
          $notify: jest.fn(),
        };
        const onSuccess = () => {
          expect(context.dispatch).toHaveBeenNthCalledWith(
            1,
            'tokens/find',
            {
              query: {
                username,
                password,
              },
            },
            { root: true },
          );
          expect(context.dispatch).toHaveBeenNthCalledWith(
            2,
            'session/create',
            { token },
            { root: true },
          );
          expect(thisArg.$notify).not.toBeCalled();
          done();
        };
        const payload = {
          username,
          password,
          onSuccess,
        };
        login.call(thisArg, context, payload);
      });

      it('mutates logoutCallback on success', async () => {
        const token = { id: 1, accessToken: 'foobar' };
        const onLogout = () => { };
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.resolve([token])),
        };
        const payload = {
          username: 'foo',
          password: 'bar',
          onSuccess: jest.fn(),
          onLogout,
        };
        await login(context, payload);

        expect(context.commit).toHaveBeenCalledWith(
          'setLogoutCallback',
          onLogout,
        );
      });

      it('notify on login error (no tokens found)', async () => {
        const username = 'producao@incuca.com.br';
        const password = 'bc3c6e15';
        const onSuccess = jest.fn();
        const thisArg = {
          $notify: jest.fn(),
        };
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.resolve()),
          rootGetters: {
            'tokens/list': [],
          },
        };
        const payload = {
          username,
          password,
          onSuccess,
        };
        await login.call(thisArg, context, payload);
        expect(onSuccess).not.toBeCalled();
        expect(thisArg.$notify).toBeCalledWith(
          expect.any(String),
        );
      });

      it('notify on login error (find call error)', async () => {
        const err = Error('foo');
        const username = 'producao@incuca.com.br';
        const password = 'bc3c6e15';
        const onSuccess = jest.fn();
        const thisArg = {
          $notify: jest.fn(),
        };
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.reject(err)),
          rootGetters: {
            'tokens/list': [],
          },
        };
        const payload = {
          username,
          password,
          onSuccess,
        };
        await login.call(thisArg, context, payload);
        expect(onSuccess).not.toBeCalled();
        expect(thisArg.$notify).toBeCalledWith(
          expect.any(String),
        );
      });
    });

    describe('checkUserFirstLogin Action', () => {
      it('sets first access state true', async () => {
        const dispatch = jest.fn();
        const commit = jest.fn();
        const gt = {
          currentUserId: 1,
        };
        const rootState = {
          users: {
            copy: {
              firstAccess: 1,
            },
          },
        };
        const state = {
          firstAccess: false,
        };
        await checkUserFirstLogin({
          dispatch, getters: gt, rootState, commit, state,
        });
        expect(dispatch).toBeCalledWith('users/get', gt.currentUserId, { root: true });
        expect(commit).toBeCalledWith('setFirstAccess', true);
      });
      it('does not set firstAccess', async () => {
        const dispatch = jest.fn();
        const commit = jest.fn();
        const gt = {
          currentUserId: 1,
        };
        const rootState = {
          users: {
            copy: {
              firstAccess: 0,
            },
          },
        };
        const state = {
          firstAccess: false,
        };
        await checkUserFirstLogin({
          dispatch, getters: gt, rootState, commit, state,
        });
        expect(commit).not.toBeCalled();
      });
    });

    describe('changeUserPassword action', () => {
      it('sets overlay', async () => {
        const commit = jest.fn();
        const $this = {
          $notify: jest.fn(),
        };
        const dispatch = jest.fn(() => Promise.resolve());
        const rootState = {
          users: {
            copy: {
              email: 'foo',
            },
          },
        };
        const gt = {
          currentUserId: 1,
        };
        const input = {
          currentPasword: 'foo',
          newPassword: 'bar',
        };
        await changeUserPassword.call($this, {
          commit, dispatch, rootState, getters: gt,
        }, input);
        expect(commit).toHaveBeenLastCalledWith('setOverlay', false);
      });
      it('calls changeFirstAccess when password changes', async () => {
        const commit = jest.fn();
        const $this = {
          $notify: jest.fn(),
        };
        const dispatch = jest.fn(() => Promise.resolve());
        const rootState = {
          passwordChanges: {
            copy: {
              status: 200,
              message: 'foo',
            },
          },
          users: {
            copy: {
              email: 'foo',
            },
          },
        };
        const gt = {
          currentUserId: 1,
        };
        const input = {
          currentPasword: 'foo',
          newPassword: 'bar',
        };
        await changeUserPassword.call($this, {
          commit, dispatch, rootState, getters: gt,
        }, input);
        expect(dispatch).toHaveBeenNthCalledWith(3, 'changeFirstAccess');
      });
      it('keeps first access unchanged when password change fails', async () => {
        const commit = jest.fn();
        const $this = {
          $notify: jest.fn(),
        };
        const dispatch = jest.fn(() => Promise.resolve());
        const rootState = {
          passwordChanges: {
            copy: {
              status: 404,
              message: 'foo',
            },
          },
          users: {
            copy: {
              email: 'foo',
            },
          },
        };
        const gt = {
          currentUserId: 1,
        };
        const input = {
          currentPasword: 'foo',
          newPassword: 'bar',
        };
        await changeUserPassword.call($this, {
          commit, dispatch, rootState, getters: gt,
        }, input);
        expect(commit).toHaveBeenNthCalledWith(1, 'setOverlay', false);
        expect($this.$notify).toHaveBeenCalledWith(expect.any(String));
      });

      // it('set first access state false if result status < 400', async () => {
      //   expect(commit)
      // });
    });

    describe('logout action', () => {
      it('removes session and call logoutCallback', async () => {
        const logoutCallback = jest.fn();
        const thisArg = {
          $notify: jest.fn(),
        };
        const context = {
          dispatch: jest.fn(() => Promise.resolve()),
          state: {
            logoutCallback,
          },
        };
        await logout.call(thisArg, context);
        expect(context.dispatch).toBeCalledWith('session/remove', {}, { root: true });
        expect(logoutCallback).toBeCalled();
      });

      it('notify if error', async () => {
        const error = new Error('foo');
        const thisArg = {
          $notify: jest.fn(),
        };
        const context = {
          state: {
            logoutCallback: jest.fn(),
          },
          dispatch() {
            throw error;
          },
        };
        await logout.call(thisArg, context);
        expect(thisArg.$notify).toBeCalledWith(error);
      });
    });

    describe('passwordReset action', () => {
      it('notify on password reset', async () => {
        const email = 'producao@incuca.com.br';
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.resolve()),
          rootState: {
            passwordResets: {
              copy: {
                id: email,
                status: 200,
                message: 'Senha alterada com sucesso',
              },
            },
          },
        };
        const thisArg = {
          $notify: jest.fn(),
        };
        const payload = { email };
        await passwordReset.call(thisArg, context, payload);
        expect(context.dispatch).toHaveBeenNthCalledWith(
          1,
          'passwordResets/create',
          { email },
          { root: true },
        );
        expect(thisArg.$notify).toBeCalledWith(
          expect.any(String),
          'info',
        );
      });

      it('notify if password reset fails', async () => {
        const email = 'producao@incuca.com.br';
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.resolve()),
          rootState: {
            passwordResets: {
              copy: {
                id: email,
                status: 400,
                message: 'fail msg whatever',
              },
            },
          },
        };
        const thisArg = {
          $notify: jest.fn(),
        };
        const payload = { email };
        await passwordReset.call(thisArg, context, payload);
        expect(context.dispatch).toHaveBeenNthCalledWith(
          1,
          'passwordResets/create',
          { email },
          { root: true },
        );
        expect(thisArg.$notify).toBeCalledWith(
          expect.any(String),
        );
      });

      it('notify if password reset fails (network error)', async () => {
        const email = 'producao@incuca.com.br';
        const context = {
          commit: jest.fn(),
          dispatch: jest.fn(() => Promise.reject()),
          rootState: {
            passwordResets: {
              copy: {
                id: email,
                status: 400,
                message: 'fail msg whatever',
              },
            },
          },
        };
        const thisArg = {
          $notify: jest.fn(),
        };
        const payload = { email };
        await passwordReset.call(thisArg, context, payload);
        expect(context.dispatch).toHaveBeenNthCalledWith(
          1,
          'passwordResets/create',
          { email },
          { root: true },
        );
        expect(thisArg.$notify).toBeCalledWith(
          expect.any(String),
        );
      });
    });
  });
});
