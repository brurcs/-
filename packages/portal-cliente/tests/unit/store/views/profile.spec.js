import profile from '@/store/views/profile';

describe('profile store', () => {
  const { findCurrentUser, updateUser } = profile.actions;
  const { currentUserId } = profile.getters;
  describe('actions', () => {
    it('Loads current user data', async () => {
      const dispatch = jest.fn();
      const commit = jest.fn();
      const getters = {
        currentUserId: 1,
      };
      const user = {};
      const rootState = {
        users: {
          copy: user,
        },
      };
      const userId = 1;
      await findCurrentUser({
        dispatch, getters, rootState, commit,
      });
      expect(dispatch).toHaveBeenCalledWith('users/get', userId, { root: true });
      expect(commit).toHaveBeenCalledWith('setUser', user);
    });
    it('update user', () => {
      const dispatch = jest.fn();
      const getters = {
        currentUserId: 1,
      };
      const data = {};
      updateUser({ dispatch, getters }, data);
      expect(dispatch).toHaveBeenCalledWith('users/patch', [getters.currentUserId, data], { root: true });
    });
  });
  describe('getters', () => {
    it('returns current user id from rootstate', () => {
      const st = {};
      const gt = {};
      const rootState = {
        session: {
          token: {
            userId: 1,
          },
        },
      };
      const result = 1;

      expect(currentUserId(st, gt, rootState)).toEqual(result);
    });
  });
});
