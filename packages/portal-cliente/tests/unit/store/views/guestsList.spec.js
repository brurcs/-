import guestsList from '@/store/views/guestsList';

describe('guestsList store', () => {
  it('has initial state', () => {
    const st = guestsList.state;
    expect(st).toEqual({
      permissions: [],
      currentAccountGuests: [],
      overlay: false,
      resetDialogCallback: null,
      updatingGuest: null,
    });
  });

  describe('mutations', () => {
    const {
      setPermissions, setCurrentAccountGuests,
      setUpdatingGuest,
    } = guestsList.mutations;

    it('mutates permissions list', () => {
      const fakeState = {
        permissions: [],
      };
      const data = [
        {
          id: 1,
          name: 'BOLETOS_NOTAS',
        },
        {
          id: 2,
          name: 'CONTROLE_USUARIOS',
        },
        {
          id: 3,
          name: 'GERIR_DOCUMENTOS',
        },
        {
          id: 4,
          name: 'ORDENS_DE_SERVIÇO',
        },
        {
          id: 5,
          name: 'EVENTOS',
        },
        {
          id: 6,
          name: 'DASHBOARD',
        },
        {
          id: 7,
          name: 'SOLICITACOES',
        },
        {
          id: 8,
          name: 'ALTERAR_SENHA',
        },
      ];
      setPermissions(fakeState, data);
      expect(fakeState.permissions).toEqual(data);
    });

    it('mutates currentAccountGuests', () => {
      const state = Object.assign({}, guestsList.state);
      const users = {};
      setCurrentAccountGuests(state, users);
      expect(state.currentAccountGuests).toEqual(users);
    });

    it('mutates updatingGuest', () => {
      const user = {};
      const st = {
        updatingGuest: null,
      };
      setUpdatingGuest(st, user);
      expect(st.updatingGuest).toBe(user);
    });
  });

  describe('getters', () => {
    const {
      permissionsSelector, isResetDialogOpen,
    } = guestsList.getters;

    it('merges permissions object to provide label roles', () => {
      const st = {
        permissions: [
          {
            id: 1,
            name: 'BOLETOS_NOTAS',
          },
          {
            id: 2,
            name: 'CONTROLE_USUARIOS',
          },
          {
            id: 3,
            name: 'GERIR_DOCUMENTOS',
          },
          {
            id: 4,
            name: 'ORDENS_DE_SERVIÇO',
          },
          {
            id: 5,
            name: 'EVENTOS',
          },
          {
            id: 6,
            name: 'DASHBOARD',
          },
          {
            id: 7,
            name: 'SOLICITACOES',
          },
        ],
      };
      const result = [
        {
          id: 1,
          name: 'BOLETOS_NOTAS',
          label: 'NFs e Boletos',
        },
        {
          id: 2,
          name: 'CONTROLE_USUARIOS',
          label: 'Controle de Usuários',
        },
        {
          id: 3,
          name: 'GERIR_DOCUMENTOS',
          label: 'Documentos',
        },
        {
          id: 4,
          name: 'ORDENS_DE_SERVIÇO',
          label: 'Segurança Eletrônica/ Ordem de Serviço',
        },
        {
          id: 5,
          name: 'EVENTOS',
          label: 'Eventos',
        },
        {
          id: 6,
          name: 'DASHBOARD',
          label: 'Estatísticas',
        },
        {
          id: 7,
          name: 'SOLICITACOES',
          label: 'Solicitações',
        },
      ];
      expect(permissionsSelector(st)).toEqual(result);
    });

    it('isResetDialogOpen returns true', () => {
      const st = {
        resetDialogCallback: () => {},
      };
      expect(isResetDialogOpen(st)).toEqual(true);
    });

    it('isResetDialogOpen returns false', () => {
      const st = {
        resetDialogCallback: null,
      };
      expect(isResetDialogOpen(st)).toEqual(false);
    });
  });

  describe('actions', () => {
    const {
      loadPermissions, loadGuests,
      patchUser, onUpdateClick, loadGuest,
    } = guestsList.actions;

    describe('loadPermissions', () => {
      it('loads permissions list', () => {
        const commit = jest.fn();
        loadPermissions({ commit });
        const data = [
          {
            id: 1,
            name: 'BOLETOS_NOTAS',
          },
          {
            id: 2,
            name: 'CONTROLE_USUARIOS',
          },
          {
            id: 3,
            name: 'GERIR_DOCUMENTOS',
          },
          {
            id: 4,
            name: 'ORDENS_DE_SERVIÇO',
          },
          {
            id: 5,
            name: 'EVENTOS',
          },
          {
            id: 6,
            name: 'DASHBOARD',
          },
          {
            id: 7,
            name: 'SOLICITACOES',
          },
        ];
        expect(commit).toHaveBeenCalledWith(
          'setPermissions',
          data,
        );
      });
    });

    describe('loadGuests', () => {
      it('loads all users from current contract', async () => {
        const managerId = 1;
        const managerAccountId = 1;
        const contractCode = '001';
        const guests = [
          { id: 123, features: [] },
        ];
        const users = [{
          id: 1,
          managerId,
          managerAccountId,
        }];
        const dispatch = jest.fn(() => Promise.resolve(users));
        const commit = jest.fn();
        const rootState = {
          session: {
            currentAccount: {
              id: managerAccountId,
              contractCode,
              userOwnerId: managerId,
              guests,
            },
          },
        };
        await loadGuests({
          dispatch, commit, rootState,
        });
        expect(dispatch).toHaveBeenCalledWith('users/find', {
          query: {
            id: { $in: [guests[0].id] },
          },
        }, { root: true });
        expect(commit).toHaveBeenCalledWith('setCurrentAccountGuests', users);
      });
    });

    describe('loadGuest', () => {
      it('set updating guest', async () => {
        const user = {};
        const commit = jest.fn();
        const id = 1;
        const dispatch = jest.fn(
          () => Promise.resolve(user),
        );
        await loadGuest({ dispatch, commit }, id);
        expect(dispatch).toBeCalledWith(
          'users/get',
          id,
          { root: true },
        );
        expect(commit).toBeCalledWith(
          'setUpdatingGuest',
          user,
        );
      });
    });

    describe('patchUser', () => {
      it('patch gateway user', async () => {
        const thisArg = {
          // $notify: jest.fn(),
        };
        const userId = 1;
        const user = {
          id: userId,
          email: 'foo@bar',
        };
        const dispatch = jest.fn();
        await patchUser.call(thisArg, { dispatch }, user);
        expect(dispatch).toBeCalledWith(
          'users/patch',
          [userId, user],
          { root: true },
        );
      });
    });

    describe('onUpdateClick', () => {
      it('dispatch patchUser', () => {
        const user = {};
        const onSuccess = () => {};
        const dispatch = jest.fn();
        const commit = jest.fn();
        onUpdateClick({ dispatch, commit }, { user, onSuccess });
        expect(dispatch).toBeCalledWith(
          'patchUser',
          user,
        );
      });

      it.skip('call success', async () => {
        // TODO: add call success test
      });

      it.skip('notify any error', async () => {
        // TODO: add notify error test
      });
    });
  });
});
