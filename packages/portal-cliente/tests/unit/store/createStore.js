/* eslint-disable import/no-extraneous-dependencies */
import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import feathersVuex from 'feathers-vuex';
import storeModule from '@/store';
import router from '@/router';
import createSession from '@/lib/session';
import feathersClient from '@/lib/feathersClient';

export default function createLocalStore() {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const { VUE_APP_GATEWAY_URI } = process.env;
  window.fetch = jest.fn();
  const client = feathersClient(VUE_APP_GATEWAY_URI);
  const session = createSession(router, client);
  const log = jest.fn();
  const { service } = feathersVuex(client, { idField: 'id' });
  const store = new Vuex.Store(storeModule({
    client,
    service,
    session,
    log,
  }));
  return store;
}
