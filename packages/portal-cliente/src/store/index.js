import notify from '@/lib/notify';
import login from './login';
import app from './app';
import views from './views';
import emailSubscription from './emailSubscription';

/**
 * Store
 *
 * @namespace store
 */
export default function setupStore({
  client, service, session, log,
}) {
  // Setup custom service timeouts (client here is feathersClient)
  /* istanbul ignore next */
  client.service('tokens').timeout = 25000;

  const plugins = [
    notify,
    session,
    service('users'),
    service('users/passwordResets'),
    service('users/passwordChanges'),
    service('users/emailSubscriptions'),
    service('tokens'),
    service('accounts'),
    service('mobileRequests'),
    service('fileRequests'),
  ];

  if (log) {
    plugins.push(log);
  }

  // return store options
  return {
    plugins,
    modules: {
      login,
      app,
      views,
      emailSubscription,
    },
  };
}
