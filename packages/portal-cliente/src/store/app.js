/* istanbul ignore file */
const state = {
  drawer: false,
  mini: false,
};
const mutations = {
  setDrawer(st, drawer) {
    st.drawer = drawer;
  },
  setMini(st, mini) {
    st.mini = mini;
  },
};
const getters = {
  globalMenu() {
    return [
      { title: 'Contato', icon: 'mdi-email', to: { name: 'ContactsAdd' } },
      // { title: 'Mensagens', icon: 'mdi-forum' },
    ];
  },
  userMenu(st, gt, rSt) {
    const allowedMenuItems = ({ featureId }) => {
      const { currentUser, currentAccount } = rSt.session;

      // menuItem without feature is always allowed
      if (!featureId) return true;

      // does not allow anything until login
      if (!currentUser || !currentAccount) return false;

      // allow anything if user is the manager
      if (currentUser.id === currentAccount.userOwnerId) return true;

      // User is an invited guest

      // does not allow anything if account has not guests
      if (!currentAccount.guests) return false;

      // Never allow guest to invite another guests
      if (featureId === 2) return false;

      // Get guest data in account
      const guest = currentAccount.guests.find(
        ({ id }) => id === currentUser.id,
      );

      // does not allow anything if guest was not found on current account
      if (!guest) return false;

      return guest
        .features
        .some(feature => feature.id === featureId);
    };
    return [
      {
        title: 'Notificações',
        icon: 'mdi-bell',
        featureId: 4,
        to: { name: 'Notifications' },
      },
      {
        title: 'Estatísticas',
        icon: 'mdi-equalizer',
        featureId: 6,
        to: { name: 'Dashboard' },
      },
      {
        title: 'NFs e Boletos',
        icon: 'mdi-barcode',
        featureId: 1,
        to: { name: 'invoices' },
      },
      {
        title: 'Documentos',
        icon: 'mdi-file-document',
        featureId: 3,
        to: { name: 'Documents' },
      },
      {
        title: 'Segurança Eletrônica',
        icon: 'mdi-shield-check',
        featureId: 5,
        to: { name: 'AlarmsList' },
      },
      {
        title: 'Solicitações',
        icon: 'mdi-alert-circle',
        featureId: 7,
        to: { name: 'ContactsList' },
      },
      // Disabled due Design QA
      // {
      //   title: 'Meu Perfil',
      //   icon: 'mdi-account',
      //   to: { name: 'Profile' },
      // },
      {
        title: 'Controle de usuários',
        icon: 'mdi-account-group',
        featureId: 2,
        to: { name: 'GuestsList' },
      },
    ].filter(allowedMenuItems);
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  getters,
};
