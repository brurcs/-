import { isIdError } from '@/lib/idError';

const state = {
  overlay: false,
  firstAccess: false,
  recEmail: '',
  logoutCallback: () => {
    window.location = '/';
  },
};

const mutations = {
  setFirstAccess(st, firstAccess) {
    st.firstAccess = firstAccess;
  },
  setLogoutCallback(st, callback) {
    st.logoutCallback = callback;
  },
  setOverlay(st, overlay) {
    st.overlay = overlay;
  },
  setRecEmail(st, param) {
    st.recEmail = param;
  },
};

const getters = {
  currentUserId(st, gt, rootState) {
    const { token } = rootState.session;
    return token && token.userId;
  },
  isPasswordDialogOpen(st) {
    return st.firstAccess;
  },
  accounts(st) {
    return st.userAccounts;
  },
};

const actions = {
  changeFirstAccess({ dispatch, commit, getters: gt }) {
    const userPatch = {
      firstAccess: false,
    };
    commit('setFirstAccess', false);
    dispatch('users/patch', [gt.currentUserId, userPatch], { root: true });
  },
  async checkUserFirstLogin({
    dispatch, getters: gt, state: st, rootState, commit,
  }) {
    await dispatch('users/get', gt.currentUserId, { root: true });
    const { firstAccess } = rootState.users.copy;
    const boolFirstAccess = !!+firstAccess;
    if (boolFirstAccess !== st.firstAccess) {
      commit('setFirstAccess', boolFirstAccess);
    }
  },
  /**
   *
   */
  async changeUserPassword({
    commit, dispatch, rootState, getters: gt,
  }, input) {
    await dispatch('users/get', gt.currentUserId, { root: true });
    const { email } = rootState.users.copy;
    const data = {
      email,
      currentPassword: input.currentPasword,
      newPassword: input.newPassword,
    };
    try {
      await dispatch('passwordChanges/create', data, { root: true });
      const result = rootState.passwordChanges.copy;
      if (result.status < 400) {
        this.$notify(result.message, 'info');
        dispatch('changeFirstAccess');
      } else {
        this.$notify('Desculpe, não foi possível alterar sua senha, verifique os dados digitados e tente novamente');
      }
    } catch (e) {
      if (isIdError(e, 'WRONG_PASSWORD')) {
        this.$notify('Senha atual incorreta, por favor verifique e tente novamente');
      } else {
        this.$notify('Erro ao trocar a senha, por favor tente novamente');
      }
    }
    commit('setOverlay', false);
  },
  /**
   *
   */
  async login({ dispatch, commit }, {
    username, password, onSuccess, onLogout,
  }) {
    const query = {
      username,
      password,
    };
    // const valida = {
      // username,
    // };
    try {
      /* if (query.username.length === 18) {
        valida.username = query.username;
        valida.username = username.replace(/[^\d]+/g, '');
        if (valida.length === 14) {
          query.username = valida.username;
        }
        const tokens = await dispatch('tokens/find', { query }, { root: true });
        if (tokens.length > 0) {
          const token = tokens[0];
          await dispatch('session/create', { token }, { root: true });
          if (onLogout) commit('setLogoutCallback', onLogout);
          dispatch('checkUserFirstLogin');
          onSuccess();
        } else {
          throw Error(' ');
        }
      } else if (query.username.length === 14) {
        valida.username = query.username;
        valida.username = username.replace(/[^0-9]/g, '');
        if (valida.length === 11) {
          query.username = valida.username;
        }
        const tokens = await dispatch('tokens/find', { query }, { root: true });
        if (tokens.length > 0) {
          const token = tokens[0];
          await dispatch('session/create', { token }, { root: true });
          if (onLogout) commit('setLogoutCallback', onLogout);
          dispatch('checkUserFirstLogin');
          onSuccess();
        } else {
          throw Error(' ');
        }
      } else {
        */
      const tokens = await dispatch('tokens/find', { query }, { root: true });
      if (tokens.length > 0) {
        const token = tokens[0];
        await dispatch('session/create', { token }, { root: true });
        if (onLogout) commit('setLogoutCallback', onLogout);
        dispatch('checkUserFirstLogin');
        onSuccess();
      } else {
        throw Error('Usuário não encontrado com os dados fornecidos, por favor verifique e tente novamente');
      }
      // }
    } catch (err) {
      this.$notify('Dados de login incorretos, tente novamente.');
    }
    commit('setOverlay', false);
  },
  async logout({ dispatch, state: st }) {
    try {
      st.logoutCallback();
      await dispatch('session/remove', {}, { root: true });
    } catch (err) {
      this.$notify(err);
    }
  },
  /**
   *
   */
  async passwordReset({ dispatch, rootState, commit }, { email }) {
    try {
      await dispatch('passwordResets/create', { email }, { root: true });
      const result = rootState.passwordResets.copy;
      if (result.status < 400) {
        this.$notify(`${result.message} para ${email}`, 'info');
        dispatch('setLogoutCallback');
      } else {
        this.$notify('Desculpe, não foi possível recuperar a senha, verifique os dados digitados e tente novamente');
      }
    } catch (e) {
      this.$notify('Erro ao resetar a senha, por favor tente novamente');
    }
    commit('setOverlay', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
