/* istanbul ignore file */

const state = {
  notifications: null,
  periodsLimit: 6,
};
const mutations = {
  setNotifications(st, notifications) {
    st.notifications = notifications;
  },
};
const getters = {
  isLoading(st) {
    return !st.notifications;
  },
  noResults(st) {
    return st.notifications && st.notifications.length === 0;
  },
  limitedPeriods(st) {
    if (!st.periods) return null;
    return st.periods.slice(0, st.periodsLimit);
  },
};
const actions = {
  async loadNotifications({ commit, dispatch, rootState }) {
    commit('setNotifications', null); // to enable loading
    const { cpfCnpjOwner } = rootState.session.currentAccount;
    const { contractCode } = rootState.session.currentAccount;
    const url = '/portalcliente/v1/rats';
    const params = {
      cgcCpf: cpfCnpjOwner,
      numPag: 0,
      contractCode,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Não foram encontradas Rats para este cliente.');
    };
    try {
      const res = await dispatch('mobileRequests/create', { url, params, method }, { root: true });
      if (res.data.status === 100 || res.data.status === 101) {
        commit('setNotifications', res.data.ret);
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
