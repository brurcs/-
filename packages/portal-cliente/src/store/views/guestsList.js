/* istanbul ignore file */
const state = {
  overlay: false,
  permissions: [],
  currentAccountGuests: [],
  updatingGuest: null,
  resetDialogCallback: null,
};

const mutations = {
  setPermissions(st, data) {
    st.permissions = data;
  },
  setCurrentAccountGuests(st, guests) {
    st.currentAccountGuests = guests;
  },
  setUpdatingGuest(st, updatingGuest) {
    st.updatingGuest = updatingGuest;
  },
  setResetDialogCallback(st, callback) {
    st.resetDialogCallback = callback;
  },
  setOverlay(st, overlay) {
    st.overlay = overlay;
  },
};

const getters = {
  permissionsSelector(st) {
    const haslabel = [
      {
        id: 1,
        label: 'NFs e Boletos',
      },
      {
        id: 2,
        label: 'Controle de Usuários',
      },
      {
        id: 3,
        label: 'Documentos',
      },
      {
        id: 4,
        label: 'Segurança Eletrônica/ Ordem de Serviço',
      },
      {
        id: 5,
        label: 'Eventos',
      },
      {
        id: 6,
        label: 'Estatísticas',
      },
      {
        id: 7,
        label: 'Solicitações',
      },
    ];

    const newData = [];

    st.permissions.forEach((itm, i) => {
      newData.push(Object.assign({}, itm, haslabel[i]));
    });

    return newData;
  },
  isResetDialogOpen(st) {
    return st.resetDialogCallback !== null;
  },
};

const actions = {
  loadPermissions({ commit }) {
    const data = [
      {
        id: 1,
        name: 'BOLETOS_NOTAS',
      },
      {
        id: 2,
        name: 'CONTROLE_USUARIOS',
      },
      {
        id: 3,
        name: 'GERIR_DOCUMENTOS',
      },
      {
        id: 4,
        name: 'ORDENS_DE_SERVIÇO',
      },
      {
        id: 5,
        name: 'EVENTOS',
      },
      {
        id: 6,
        name: 'DASHBOARD',
      },
      {
        id: 7,
        name: 'SOLICITACOES',
      },
    ];
    commit('setPermissions', data);
  },
  async loadGuests({
    dispatch, rootState, commit,
  }) {
    const { currentAccount } = rootState.session;
    const { guests } = currentAccount;
    if (guests && guests.length > 0) {
      const users = await dispatch('users/find', {
        query: {
          id: { $in: guests.map(({ id }) => id) },
        },
      }, { root: true });
      commit(
        'setCurrentAccountGuests',
        users,
      );
    } else {
      commit('setCurrentAccountGuests', []);
    }
  },
  async loadGuest({ dispatch, commit }, id) {
    const user = await dispatch('users/get', id, { root: true });
    commit('setUpdatingGuest', user);
  },
  createUser({ dispatch, rootState }, user) {
    const { id, userOwnerId, contractCode } = rootState.session.currentAccount;
    const addingUser = {
      ...user,
      managerId: userOwnerId,
      accountId: id,
      contractCode,
      firstAccess: true,
    };
    return dispatch('users/create', [addingUser], { root: true });
  },
  patchUser({ dispatch }, userData) {
    return dispatch('users/patch', [userData.id, userData], { root: true });
  },
  updateAccountGuest({ dispatch, rootState }, guest) {
    const { currentAccount } = rootState.session;

    const { guests } = currentAccount;

    // remove given guest
    const newGuests = (guests && guests.filter(
      g => g.id !== guest.id,
    )) || [];

    // patch persisted guest
    const patchedGuest = {
      ...guests.find(({ id }) => id === guest.id) || {},
      ...guest,
    };

    // push patched guest
    newGuests.push(patchedGuest);
    return dispatch(
      'accounts/patch',
      [currentAccount.id, { guests: newGuests }],
      { root: true },
    );
  },
  async onAddClick({ dispatch, commit }, { user, features, onSuccess }) {
    try {
      commit('setOverlay', true);
      const ret = await dispatch('createUser', { ...user, features });
      if (ret.$orsIdMessage === 'Nova conta e produto vinculados ao usuario com sucesso.') {
        commit('setOverlay', false);
        // $notify(ret.$orsIdMessage, 'success');
      } else {
        commit('setOverlay', false);
        // $notify('Usuário adicionado com sucesso', 'success');
      }
      await dispatch('updateAccountGuest', { id: ret.id, features });
      onSuccess();
    } catch (e) {
      if (e.data && e.data.idError && e.data.idError.error === 'USER_EXIST_ACCOUNTS') {
        commit('setOverlay', false);
        commit('setResetDialogCallback', () => {
          dispatch('login/passwordReset', user, { root: true });
          onSuccess();
        });
      } else {
        commit('setOverlay', false);
        // $notify('Tivemos um problema em nosso servidor, contate o suporte.');
      }
    }
  },
  async onUpdateClick({ dispatch, commit }, { user, features, onSuccess }) {
    try {
      commit('setOverlay', true);
      await dispatch('patchUser', user);
      await dispatch('updateAccountGuest', { id: user.id, features });
      commit('setOverlay', false);
      // $notify('Convidado atualizado com sucesso', 'success');
      onSuccess();
    } catch (e) {
      commit('setOverlay', false);
      // $notify('Erro ao atualizar convidado.');
    }
  },
  async onDeleteGuest({ dispatch, rootState, commit }, { item, onDeleteFinally }) {
    const { currentAccount } = rootState.session;
    try {
      commit('setOverlay', true);
      await dispatch('accounts/patch', [currentAccount.id, { delete: true, user: item }], { root: true });
      await dispatch('loadGuests');
      onDeleteFinally();
      commit('setOverlay', false);
      // $notify('Convidado removido com sucesso', 'success');
    } catch (e) {
      commit('setOverlay', false);
      // this.$notify(e);
      onDeleteFinally();
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
