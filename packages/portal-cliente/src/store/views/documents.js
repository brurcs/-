/* istanbul ignore file */
import mergeDeep from '@/lib/mergeDeep';

const state = {
  periods: null,
  periodsLimit: 6,
};
const mutations = {
  setPeriods(st, periods) {
    st.periods = periods;
  },
  setPeriodsLimit(st, limit) {
    st.periodsLimit = limit;
  },
};
const getters = {
  limitedPeriods(st) {
    if (!st.periods) return null;
    return st.periods.slice(0, st.periodsLimit);
  },
  isLoading(st, gt) {
    return !gt.limitedPeriods;
  },
  noResults(st, gt) {
    return gt.limitedPeriods && gt.limitedPeriods.length === 0;
  },
  noMoreResults(st) {
    if (!st.periods) return true;
    return st.periods.length - st.periodsLimit <= 0;
  },
};
const actions = {
  async loadPeriods({ commit, dispatch, rootState }) {
    commit('setPeriods', null); // to enable loading

    const url = '/portalcliente/v1/documentos';
    const { cpfCnpjOwner } = rootState.session.currentAccount;
    const params = {
      cgcCpf: cpfCnpjOwner,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Erro ao carregar competências.');
    };
    try {
      const res = await dispatch('mobileRequests/create', { url, params, method }, { root: true });
      if (res.data.status === 100) {
        // FIXME: assuming ret[0]
        const ret = res.data.ret[0];
        const companies = ret.empresas;
        // FIXME: assuming that companies should be merged
        // FIXME: assuming that periods from companies should be merged
        const periods = companies.reduce(
          (result, company) => mergeDeep(result, company.arquivos),
          {},
        );

        // turn into compatible array of objects
        const periodsArray = Object.keys(periods)
          .map(
            perKey => ({ competencia: perKey, arquivos: periods[perKey] }),
          )
          .sort((prev, next) => prev.competencia - next.competencia)
          .map(period => ({
            competencia: period.competencia,
            arquivos: period.arquivos.slice(0, 3),
            ultimosArquivos: period.arquivos.slice(3),
            show: false,
          }));
        commit('setPeriods', periodsArray);
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
  loadMorePeriods({ commit, state: st }) {
    commit('setPeriodsLimit', st.periodsLimit + 6);
  },
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
