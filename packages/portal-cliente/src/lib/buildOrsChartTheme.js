import { color, registry } from '@amcharts/amcharts4/core';

function is(object, name) {
  const x = registry.registeredClasses[name];
  return x != null && object instanceof x;
}

export default (chartType = 1) => function orsAmChartsTheme(object) {
  const colorForType = {
    1: '#078b75',
    2: '#4781bf',
    3: '#de7c00',
  };
  if (is(object, 'ColorSet')) {
    object.list = [color(colorForType[chartType])];
  }
  if (is(object, 'InterfaceColorSet')) {
    object.setFor('text', color('#7d7d7d'));
    object.setFor('secondaryButton', color('#FCE300'));
    object.setFor('secondaryButtonHover', color('#FCE300').lighten(-0.2));
    object.setFor('secondaryButtonDown', color('#FCE300').lighten(-0.2));
    object.setFor('secondaryButtonActive', color('#FCE300').lighten(-0.2));

    object.setFor('primaryButton', color(colorForType[chartType]));
    object.setFor('primaryButtonHover', color(colorForType[chartType]).lighten(-0.2));
    object.setFor('primaryButtonDown', color(colorForType[chartType]).lighten(-0.2));
    object.setFor('primaryButtonActive', color(colorForType[chartType]).lighten(-0.2));
  }
  if (is(object, 'ResizeButton')) {
    object.background.cornerRadiusTopLeft = 20;
    object.background.cornerRadiusTopRight = 20;
    object.background.cornerRadiusBottomLeft = 20;
    object.background.cornerRadiusBottomRight = 20;
  }
  if (is(object, 'Tooltip')) {
    object.animationDuration = 800;
  }

  if (is(object, 'LineSeries') && object.tooltip) {
    object.tooltip.getFillFromObject = false;
    object.tooltip.background.fill = color('#ffffff');
    object.tooltip.background.stroke = color(colorForType[chartType]);
    object.tooltip.label.fill = color(colorForType[chartType]);
  }
};
