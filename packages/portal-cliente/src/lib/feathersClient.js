import feathers from '@feathersjs/feathers';
import rest from '@feathersjs/rest-client';
import axios from 'axios';

export default (url) => {
  const restClient = rest(url);
  return feathers().configure(restClient.axios(axios));
};
