import axios from 'axios';

export default {
  get() {
    const url = process.env.VUE_APP_ORSEGUPSID_URI;
    return axios.get(url);
  },
};
