import { Forbidden } from '@feathersjs/errors';

/**
 * Allow only given methods, if a method is not allowed it will throw Forbidden
 *
 * @export allowMethods
 * @param {String} provider feathersjs provider (rest, socketio, ...)
 * @param {Array} methods list of allowed methods, all other methods will not be allowed
 * @returns {Function} the hook
 */
export default function allowMethods(provider, methods) {
  return (context) => {
    const { method, params } = context;

    if (params.provider === provider) {
      if (!methods.includes(context.method)) {
        throw new Forbidden(`Method ${method} is not allowed`);
      }

      context.methods = methods;
    }

    return context;
  };
}
