import updateIfExists from './updateIfExists';

describe('updateIfExists', () => {
  it('skip if keys are not given', () => {
    const service = {
      find: jest.fn(),
    };
    const context = {
      service,
    };
    const hook = updateIfExists([]);
    hook(context);
    expect(service.find).not.toBeCalled();
  });
  it('skip if keys are undefined', () => {
    const service = {
      find: jest.fn(() => Promise.resolve()),
    };
    const data = { foo: undefined };
    const context = {
      service,
      data,
    };
    const hook = updateIfExists(['foo']);
    hook(context);
    expect(service.find).toBeCalled();
  });
});
