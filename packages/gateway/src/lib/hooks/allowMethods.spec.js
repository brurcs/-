import { Forbidden } from '@feathersjs/errors';
import allowMethods from './allowMethods';

describe('allowMethods hook', () => {
  it('returns a hook', () => {
    expect(allowMethods('foo', ['bar'])).toBeInstanceOf(Function);
  });

  describe('hook function', () => {
    it('skip if current provider does not match given provider', () => {
      const context = {
        method: 'foo',
        params: {
          provider: 'bar',
        },
      };
      const oldContext = { ...context };
      const runHook = () => {
        const hook = allowMethods('noop', ['foo']);
        hook(context);
      };
      expect(runHook).not.toThrow();
      expect(context).toEqual(oldContext);
    });

    it('change context methods if provider is matching', () => {
      const context = {
        method: 'foo',
        params: {
          provider: 'bar',
        },
        methods: [],
      };
      const runHook = () => {
        const hook = allowMethods('bar', ['foo']);
        hook(context);
      };
      expect(runHook).not.toThrow();
      expect(context.methods).toEqual(['foo']);
    });

    it('throws Forbidden if current method does not match given methods', () => {
      const context = {
        method: 'foo',
        params: {
          provider: 'bar',
        },
        methods: [],
      };
      const runHook = () => {
        const hook = allowMethods('bar', ['thing']);
        hook(context);
      };
      expect(runHook).toThrow(Forbidden);
    });
  });
});
