export default function isTokenExpired(token) {
  if (token && token.expiresIn && token.createdAt) {
    const { expiresIn, createdAt } = token;
    const expireDate = new Date(createdAt.valueOf());
    expireDate.setMilliseconds(
      expireDate.getMilliseconds() + expiresIn * 1000,
    );
    return expireDate <= new Date();
  }
  return true;
}
