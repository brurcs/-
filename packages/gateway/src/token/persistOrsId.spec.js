import app from '@/app';
import { resetTables } from '@/testUtil';
import persistOrsId from './persistOrsId';

describe('persistOrsId', () => {
  beforeEach(
    () => resetTables(app),
  );

  it('skip if orsegupsID is not in context', async () => {
    jest.setTimeout(30000);
    const context = {
      app,
    };
    const ctx = await persistOrsId(context);
    expect(ctx).toBe(context);
  });

  it('persists token, user and accounts if orsegupsID is found in context', async () => {
    const accounts = [
      {
        id: 1,
        contractCode: '001',
        empresaContrato: '001',
        filialcontrato: '001',
        isActive: 1,
        cpfCnpjOwner: '001',
        identifier: 'foo',
        userOwnerId: 1,
      },
    ];
    const context = {
      app,
      service: app.service('tokens'),
      params: {
        query: {
          username: 'foo',
          password: 'bar',
        },
      },
      orsegupsID: {
        access_token: 'foo',
        expires_in: 900,
        user: {
          id: 1,
          name: 'foo',
          email: 'foo@bar',
          cpfCnpj: '111',
          phone: '222',
          firstAccess: 1,
          active: 1,
        },
        accounts,
      },
    };
    const ctx = await persistOrsId(context);
    expect(ctx.result[0]).toMatchObject({
      accessToken: 'foo',
      expiresIn: 900,
      userId: expect.any(Number),
    });
    const users = app.service('users');
    const accountsService = app.service('accounts');
    const user = await users.get(ctx.result[0].userId);
    const savedAccounts = await accountsService.find({
      query: { userOwnerId: user.id },
    });
    expect(user).toMatchObject({
      id: user.id,
      name: 'foo',
      email: 'foo@bar',
      cpfCnpj: '111',
      phone: '222',
      firstAccess: 1,
      active: 1,
      isAdmin: 1,
    });
    expect(savedAccounts).toHaveLength(1);
    expect(savedAccounts[0]).toMatchObject({
      contractCode: '001',
      empresaContrato: '001',
      filialContrato: '001',
      isActive: 1,
      userOwnerId: 1,
      cpfCnpjOwner: '001',
      identifier: 'foo',
    });
  });

  it('if context has storedUser the new user must have isAdmin from storedUser', async () => {
    const context = {
      app,
      service: app.service('tokens'),
      params: {
        query: {
          username: 'asd',
          password: 'qwe',
        },
      },
      orsegupsID: {
        access_token: 'foo',
        expires_in: 900,
        user: {
          id: 1,
          name: 'qwd',
          email: 'qwd@dgs',
          cpfCnpj: '124',
          phone: '123',
          firstAccess: 1,
          active: 1,
        },
        accounts: [],
      },
      storedUser: {
        isAdmin: 0,
      },
    };
    const ctx = await persistOrsId(context);
    const users = app.service('users');
    const user = await users.get(ctx.result[0].userId);
    expect(user).toMatchObject({
      isAdmin: context.storedUser.isAdmin,
    });
  });
});
