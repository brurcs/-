import feathersSequelize from 'feathers-sequelize';
import TokenService from './token.service';
import TokenModel from './token.model';
import TokenSetup from '.';
import TokenHooks, { globalHooks } from './token.hooks';
import setupServerToken from './serverToken';

jest.mock('feathers-sequelize');
jest.mock('./token.service');
jest.mock('./token.model');
jest.mock('./serverToken');

describe('tokenSetup', () => {
  const hooks = jest.fn();
  const app = {
    service: jest.fn(() => ({ hooks })),
    use: jest.fn(),
    hooks: jest.fn(),
  };

  it('enable service on /tokens', () => {
    const Model = {};
    const service = { Model };
    TokenModel.mockImplementation(() => Model);
    feathersSequelize.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    TokenService.mockImplementation(({ model = Model }) => {
      expect(model).toBe(Model);
      return service;
    });
    TokenSetup(app);
    expect(app.use).toBeCalledWith('/tokens', service);
  });

  it('set hooks for service', () => {
    TokenSetup(app);
    expect(hooks).toBeCalledWith(TokenHooks);
  });

  it('set hooks for app', () => {
    TokenSetup(app);
    expect(app.hooks).toBeCalledWith(globalHooks);
  });

  it('setup server token', () => {
    TokenSetup(app);
    expect(setupServerToken).toBeCalledWith(app);
  });
});
