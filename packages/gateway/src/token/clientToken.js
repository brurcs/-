import { NotAuthenticated } from '@feathersjs/errors';
import { logger } from '@/logger';
import isTokenExpired from './isTokenExpired';

export default function clientToken({ bypassServices = [] } = {}) {
  return async (context) => {
    const {
      service, app, params, path,
    } = context;

    // bypass given services
    const isContextService = serviceName => app.service(serviceName) === service;
    if (bypassServices.some(isContextService)) {
      logger.debug('clientToken hook bypassing %s service', path);
      return context;
    }

    // bypass skipAuth
    if (params.skipAuth) {
      logger.debug('clientToken hook skipping %s service', path);
      return context;
    }

    // throws if accessToken was not given
    if (!params.query || !params.query.accessToken) {
      logger.debug('clientToken hook empty accessToken in %s service', path);
      throw new NotAuthenticated('Token de acesso não encontrado na requisição');
    }
    const { accessToken } = params.query;

    // throws if accessToken does not exist in db
    const tokensService = app.service('tokens');
    const tokens = await tokensService.find({
      query: { accessToken },
      skipAuth: true,
    });
    if (tokens.length < 1) {
      logger.debug('clientToken hook invalid token in %s service', path);
      throw new NotAuthenticated('Token inválido');
    }
    const token = tokens[0];

    // throws if token is expired
    if (isTokenExpired(token)) {
      logger.debug('clientToken hook expired token in %s service', path);
      throw new NotAuthenticated('Token expirado');
    }

    // delete accessToken from query
    logger.debug('clientToken hook valid token in %s service', path);
    params.clientToken = token;
    delete params.query.accessToken;

    return context;
  };
}
