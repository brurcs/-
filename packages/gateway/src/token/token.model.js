import Sequelize from 'sequelize';

/**
 * Token Model
 *
 * @name TokenModel
 * @memberof module:Token
 * @param {Feathers} app
 * @returns {TokenModel}
 */
export default function TokenModel(app) {
  /**
   * @typedef {module:Token.TokenModel}
   * @property {Sequelize.INTEGER} id
   * @property {Sequelize.BIGINT} userId
   * @property {Sequelize.TEXT} accessToken
   * @property {Sequelize.INTEGER} expiresIn
   * @property {Sequelize.DATE} createdAt
   * @property {Sequelize.DATE} updatedAt
   */
  const Token = app.db.define('Token', {
    accessToken: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    expiresIn: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
  });

  Token.associate = (models) => {
    Token.belongsTo(models.User, {
      foreignKey: 'userId',
    });
  };

  return Token;
}
