/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Tokens', 'expiresIn', {
    type: Sequelize.INTEGER,
    allowNull: false,
  }),
  down: (QI, Sequelize) => QI.removeColumn('Tokens', 'expiresIn'),
};
