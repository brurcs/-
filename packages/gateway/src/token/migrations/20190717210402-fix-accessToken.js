/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.changeColumn('Tokens', 'accessToken', {
    type: Sequelize.TEXT,
  }),
  down: (QI, Sequelize) => QI.changeColumn('Tokens', 'accessToken', {
    type: Sequelize.STRING,
  }),
};
