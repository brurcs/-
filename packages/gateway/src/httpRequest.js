/* istanbul ignore file */
import axios from 'axios';
// import sslRootCAs from 'ssl-root-cas/latest';
// import path from 'path';
// import fs from 'fs';
// import https from 'https';
import { logger } from '@/logger';

// setup certificates
// https.globalAgent.options.ca = sslRootCAs.create();

export default function setupRequest(app) {
  try {
    // const ssl = app.get('ssl');
    // const cert = fs.readFileSync(path.resolve(__dirname, '../config/', ssl.cert));
    // const key = fs.readFileSync(path.resolve(__dirname, '../config/', ssl.key));
    // const { ca } = https.globalAgent.options;
    // FIXME: httpsAgent seems to not be working
    // const httpsAgent = new https.Agent({
    //   ca, cert, key,
    // });
    // app.httpRequest = axios.create({ httpsAgent });
    app.httpRequest = axios.create();
  } catch (e) {
    logger.error(
      'error configuring request (axios), details: %s',
      e,
    );
  }
}
