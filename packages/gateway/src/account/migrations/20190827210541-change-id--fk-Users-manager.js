/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users DROP FOREIGN KEY Users_managerAccountId_foreign_idx, MODIFY managerAccountId BIGINT UNSIGNED NULL;',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users ADD CONSTRAINT Users_managerAccountId_foreign_idx FOREIGN KEY(managerAccountId) REFERENCES Accounts(id), MODIFY managerAccountId INT(11) NULL;',
  ),
};
