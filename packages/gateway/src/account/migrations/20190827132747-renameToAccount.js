/* eslint-disable no-unused-vars */
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.renameTable('Contracts', 'Accounts'),
  down: (queryInterface, Sequelize) => queryInterface.renameTable('Accounts', 'Contracts'),
};
