/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'LOCK TABLES SequelizeMeta WRITE, Accounts WRITE, Users WRITE;',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'UNLOCK TABLES;',
  ),
};
