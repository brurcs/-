import Sequelize from 'sequelize';

/**
 * User Model
 *
 * @name UserModel
 * @memberof module:User
 * @param {Feathers} app
 * @returns {UserModel}
 */
export default function UserModel(app) {
  /**
   * @typedef {module:User.UserModel}
   * @property {Sequelize.INTEGER} id
   * @property {Sequelize.BIGINT} accountId Id da conta a que este
   *                                               convidado será vinculado (obrigatório
   *                                               quando managerId for definido)
   * @property {Sequelize.STRING} name             Campo obrigatório
   * @property {Sequelize.STRING} email            Campo obrigatório
   * @property {Sequelize.STRING} cpfCnpj          Atributo importante pois é uma das
   *                                               chaves para login (Não obrigatório)
   * @property {Sequelize.STRING} phone            Atributo importante pois é uma das chaves
   *                                               do login (Não obrigatório)
   * @property {Sequelize.STRING} whatsPhone       Whatsapp
   * @property {Sequelize.STRING} password         Campo obrigatório
   * @property {Sequelize.BOOLEAN} firstAccess     Flag utilizada para validar se é o
   *                                               primeiro acesso do usuário ou não. Caso
   *                                               seja, pode-se forçar a alteração de senha
   *                                               no primeiro login (No cadastro será setado
   *                                               por padrão 1)
   * @property {Sequelize.BOOLEAN} active          Flag de usuário ativo ou inativo. Padrão 1
   *                                               (Ativo).
   * @property {Sequelize.BOOLEAN} isAdmin         Se o usuário a ser cadastrado é admin ou
   *                                               não. Flag true ou false (useless for now)
   * @property {Sequelize.JSON} emailSubscriptions  Lista de cpjCnpjs que o usuário se
   *                                              inscreveu para notificações de email
   */
  const User = app.db.define('User', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    cpfCnpj: {
      type: Sequelize.STRING,
    },
    phone: {
      type: Sequelize.STRING,
    },
    whatsPhone: {
      type: Sequelize.STRING,
    },
    login: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    firstAccess: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    isAdmin: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
    emailSubscriptions: {
      type: Sequelize.JSON,
      defaultValue: [],
    },
  }, {
    timestamps: false,
  });

  User.associate = (models) => {
    User.belongsTo(models.User, {
      // deprecated use Account.userOwnerId instead
      foreignKey: 'managerId',
    });
  };

  return User;
}
