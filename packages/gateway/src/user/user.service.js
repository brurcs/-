import feathersSequelize from 'feathers-sequelize';

export default function UserService(settings) {
  return feathersSequelize(settings);
}
