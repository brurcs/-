/* eslint-disable camelcase */
import axios from 'axios';
import { logger } from '@/logger';
import OrsegupsIdError from '@/lib/OrsegupsIdError';

export default function () {
  return async function patchOrsIdUser(ctx) {
    const { app, params } = ctx;
    if (params.provider) {
      // request comes from outside (REST, socket, etc)
      const orsegupsId = app.get('orsegupsId');
      const { accessToken } = params.clientToken;
      const endpoint = `${orsegupsId.uri}/user/updateuser`;
      logger.debug('patching %s on orsegups id', ctx.data.id);
      try {
        const res = await axios.patch(
          endpoint,
          {
            id: ctx.data.id,
            ...ctx.data,
            // never send email updates
            // it's hardcoded because ors id validations
            email: undefined,
          }, {
            withCredentials: true,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `bearer ${accessToken}`,
            },
          },
        );
        const idUser = res.data.data;
        logger.debug('user %s patched, id user', ctx.data.email, idUser);
      } catch (err) {
        const idError = err.response && new OrsegupsIdError(err);
        logger.error(
          'error patching user %s on orsegups id',
          ctx.data.id,
        );
        throw idError || err;
      }
    }
    return ctx;
  };
}
