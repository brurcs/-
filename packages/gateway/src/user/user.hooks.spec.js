import updateIfExists from '../lib/hooks/updateIfExists';
// import triggerConflictForRegisteredUser from './triggerConflictForRegisteredUser';
import inviteOrsIdUser from './inviteOrsIdUser';
import patchOrsIdUser from './patchOrsIdUser';
import passwordHash from './passwordHash.hook';

jest.mock(
  '../lib/hooks/updateIfExists',
  () => jest.fn(() => 'updateIfExists'),
);
jest.mock(
  './inviteOrsIdUser',
  () => jest.fn(() => ({
    beforeCreate: 'inviteOrsIdUser',
    afterCreate: 'inviteOrsIdUser',
  })),
);
jest.mock(
  './patchOrsIdUser',
  () => jest.fn(() => 'patchOrsIdUser'),
);

/* eslint-disable global-require */
describe('account hooks', () => {
  it('register hooks', () => {
    const hooks = require('./user.hooks').default;
    expect(updateIfExists).toBeCalledWith(
      ['id'],
      true,
    );
    expect(inviteOrsIdUser).toBeCalled();
    expect(patchOrsIdUser).toBeCalled();
    expect(hooks).toEqual({
      before: {
        create: [
          // 'triggerConflictForRegisteredUser',
          'inviteOrsIdUser',
          'updateIfExists',
          passwordHash,
        ],
        patch: [
          'patchOrsIdUser',
          passwordHash,
        ],
        update: [
          passwordHash,
        ],
      },
      after: {
        create: [
          'inviteOrsIdUser',
        ],
      },
    });
  });
});
