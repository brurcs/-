/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts ADD CONSTRAINT Accounts_userOwnerId_foreign_idx FOREIGN KEY(userOwnerId) REFERENCES Users(id);',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Accounts DROP FOREIGN KEY Accounts_userOwnerId_foreign_idx;',
  ),
};
