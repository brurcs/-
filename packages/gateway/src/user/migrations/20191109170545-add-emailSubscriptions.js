/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'emailSubscriptions', {
    type: Sequelize.JSON,
    defaultValue: [],
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'emailSubscriptions'),
};
