/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.addColumn('Users', 'managerId', {
    type: Sequelize.INTEGER,
    references: {
      model: 'Users',
      key: 'id',
    },
  }),
  down: (QI, Sequelize) => QI.removeColumn('Users', 'managerId'),
};
