/* eslint-disable no-unused-vars */
module.exports = {
  up: (QI, Sequelize) => QI.changeColumn('Users', 'email', {
    type: Sequelize.STRING,
    allowNull: true,
  }),
  down: (QI, Sequelize) => QI.changeColumn('Users', 'email', {
    type: Sequelize.STRING,
    allowNull: false,
  }),
};
