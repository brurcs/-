import doc from './doc';

describe('message doc', () => {
  it('has defined object', () => {
    expect(doc).toMatchObject({
      description: expect.any(String),
      definitions: expect.any(Object),
    });
  });
});
