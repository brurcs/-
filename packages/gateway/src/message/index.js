// Initializes the `messages` service on path `/messages`
import createService from 'feathers-memory';
// import hooks from './hooks';
import docs from './doc';

export default function setupMessage(app) {
  const paginate = app.get('paginate');

  const options = {
    name: 'messages',
    paginate,
  };

  // Initialize our service with any options it requires
  app.use('/messages', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('messages');

  // service.hooks(hooks);
  service.docs = docs;
}
