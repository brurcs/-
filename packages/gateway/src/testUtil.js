export async function resetTables(app) {
  const { sequelize } = app.db.queryInterface;
  const transaction = await sequelize.transaction();
  await sequelize.query('SET FOREIGN_KEY_CHECKS = 0;', { transaction });
  await Promise.all(Object.values(sequelize.models).map(
    model => model.destroy({ truncate: true, transaction }),
  ));
  await sequelize.query('SET FOREIGN_KEY_CHECKS = 1;', { transaction });
}
