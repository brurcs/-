import { logger } from '@/logger';
import setupJobs from '.';

jest.mock('@/logger');

describe('jobs', () => {
  describe('register', () => {
    it('logs when registered', () => {
      const app = {
        use: jest.fn(),
      };
      setupJobs(app);
      app.jobs.register('foo', () => Promise.resolve());
      expect(logger.debug).toBeCalledWith(
        'job:foo registered',
      );
    });

    it('adds a middleware', () => {
      const app = {
        use: jest.fn(),
      };
      setupJobs(app);
      app.jobs.register('foo', () => Promise.resolve());
      expect(app.use).toBeCalledWith(
        expect.any(Function),
      );
    });

    describe('added middleware', () => {
      it('skip if request is not /jobs', (done) => {
        const app = {
          use: (middleware) => {
            middleware({ path: '/foo' }, {}, done);
          },
        };
        setupJobs(app);
        app.jobs.register('foo', () => Promise.resolve());
      });

      it('logs and send status when success', (done) => {
        const next = jest.fn();
        const res = {
          status: jest.fn(),
          send: jest.fn(),
        };
        const assert = () => {
          expect(logger.debug).toHaveBeenNthCalledWith(
            1,
            'job:foo started',
          );
          expect(logger.debug).toHaveBeenNthCalledWith(
            2,
            'job:foo sending 200',
          );
          expect(logger.debug).toHaveBeenNthCalledWith(
            3,
            'job:foo finished',
          );
          expect(res.status).toBeCalledWith(200);
          expect(res.send).toBeCalled();
          expect(next).not.toBeCalled();
          done();
        };

        const app = {
          use: (middleware) => {
            logger.debug.mockClear();
            middleware({ path: '/jobs/foo' }, res, next)
              .then(assert)
              .catch(done);
          },
        };
        setupJobs(app);
        app.jobs.register('foo', () => Promise.resolve());
      });

      it('logs and send status when fail', (done) => {
        const error = new Error('foo');
        const next = jest.fn();
        const res = {
          status: jest.fn(),
          send: jest.fn(),
        };
        const assert = () => {
          expect(logger.debug).toHaveBeenNthCalledWith(
            1,
            'job:foo started',
          );
          expect(logger.error).toHaveBeenCalledWith(
            'job:foo sending 500',
            error,
          );
          expect(logger.debug).toHaveBeenNthCalledWith(
            2,
            'job:foo finished',
          );
          expect(res.status).toBeCalledWith(500);
          expect(res.send).toBeCalled();
          expect(next).not.toBeCalled();
          done();
        };

        const app = {
          use: (middleware) => {
            logger.debug.mockClear();
            middleware({ path: '/jobs/foo' }, res, next)
              .then(assert)
              .catch(done);
          },
        };
        setupJobs(app);
        app.jobs.register('foo', () => Promise.reject(error));
      });
    });
  });
});
