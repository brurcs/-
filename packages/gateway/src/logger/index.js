import express from '@feathersjs/express';
import hooks from './hooks';
import winstonLogger from './winstonLogger';

export const logger = winstonLogger;

export default function setupLogger(app) {
  app.use(express.errorHandler({ logger }));
  app.hooks(hooks);
}
