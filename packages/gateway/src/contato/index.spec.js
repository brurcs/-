import ContatoService from './contato.service';
import setupContato from '.';

jest.mock('./contato.service');

describe('contatoSetup', () => {
  const app = {
    use: jest.fn(),
  };

  it('enable service on /contato', () => {
    const service = {};
    ContatoService.mockImplementation(() => service);
    setupContato(app);
    expect(app.use).toBeCalledWith('/contatos', service);
  });
});
