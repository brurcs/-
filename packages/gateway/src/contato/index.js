import createContato from './contato.service';

export default function setupContato(app) {
  // Initialize our service with any options it requires
  app.use('/contatos', createContato());
}
