import { EventEmitter } from 'events';

export default {
  use: jest.fn(),
  get(param) {
    if (param === 'port') return '3000';
    if (param === 'host') return 'localhost';
    return undefined;
  },
  listen: jest.fn(() => {
    const fakeServer = new EventEmitter();
    // for testing porpuses (we use through global proccess)
    const emitGlobalFakeServerEvent = () => {
      process.emit('fakeServerListening');
    };
    fakeServer.on(
      'listening',
      // emit after all registered listeners
      () => process.nextTick(emitGlobalFakeServerEvent),
    );

    // trigger listening event on fakeServer
    setImmediate(() => fakeServer.emit('listening'));

    return fakeServer;
  }),
};
